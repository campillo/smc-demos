%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================

function demo_1D_res()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (C) Fabien Campillo - INRIA
%--------------------------------------------------------------------------
% A simple 1D nonlinear filtering problem in 3D animation. This demo should
% be run with a few particles (here 5). It shows the basic mechanism of the
% SMC:
%  - prediction: the particle explore the space
%  - computation of the weight thru the local likelihood function
%  - resampling according to these weights
% This procedure is associated with demo_1D_nores, where there is no
% resampling (and which fails severely).
%--------------------------------------------------------------------------
% created on 8 June 2003 - modified on January 16, 2009
% - Jan 16, 2009 : cleaning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global NMAX NPRED ALPHA ALPHAM ALPHAP DT XINI VINI SIGMA2W SIGMA2V...
    SEEDIN NP SIGMAV SIGMAW DDT XMIN XMAX YMIN YMAX ZMIN ...
    ZMAX


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NMAX    = 5;         % number of time steps
NPRED   = 70;        % number of time steps for prediction/simulation
ALPHA   = 40.;       % state function parameter
ALPHAM  = 1;         % observation function parameter
ALPHAP  = 0.1;       %     "
DT      = 0.00008;   % time step
XINI    = 0.;        % mean of the initial Gaussian law
VINI    = 1;         % variance of the initial Gaussian law
SIGMA2W = 80 ;       % variance of the state noise
SIGMA2V = 80;        % variance of the observation noise
SEEDIN  = [1 ; 6 ];  % seed of the generator
NP      = 5;         % number of particles
YMIN    = -3;        % min state value (for the graphic)
YMAX    = 2;         % max state value (for the graphic)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SIGMA2V = SIGMA2V*DT;
SIGMAV  = sqrt(SIGMA2V);
SIGMA2W = SIGMA2W*DT;
SIGMAW  = sqrt(SIGMA2W);
DDT     = sqrt(DT);
XMIN    = 1;
XMAX    = NMAX*NPRED;
ZMIN    = 0;
ZMAX    = 1;

% --- simulation state process and observation process
[x,y,seedout]=simu;
% --- filtering
[position,weights,weightcor,indices]=filter(x,y);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,y,seedout]=simu()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% simulation of the state and the observation processes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global NMAX NPRED ALPHA ALPHAM ALPHAP DT XINI VINI SIGMA2W SIGMA2V...
    SEEDIN NP SIGMAV SIGMAW DDT XMIN XMAX YMIN YMAX ZMIN ...
    ZMAX
x=[];
y=[];
randn('state',SEEDIN); % random generator initialization
xold = XINI+sqrt(VINI)*randn;
for n=1:NMAX,
    for nn=1:NPRED
        xnew = xold - ALPHA*DT*xold*(xold*xold-1)+SIGMAW*randn;
        xold = xnew;
        x    = [x xnew];
        if xnew<0
            yy = ALPHAM*xnew + SIGMAV*randn;
        else
            yy = ALPHAP*xnew + SIGMAV*randn;
        end
    end
    y = [y yy];
end
seedout = randn('state'); % we keep the seed as an output

function [positions,weights,weightcor,indices]=filter(x,y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% particle filtering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global NMAX NPRED ALPHA ALPHAM ALPHAP DT XINI VINI SIGMA2W SIGMA2V...
    SEEDIN NP SIGMAV SIGMAW DDT XMIN XMAX YMIN YMAX ZMIN ...
    ZMAX

% --- filter initilialization

position_cor(1:NP) = XINI+sqrt(VINI)*randn(1,NP);
weight_cor(1:NP)   = 1/NP;


positions = zeros(NMAX,NP);
weights   = zeros(NMAX,NP);
weightcor = zeros(NMAX,NP);
indices   = zeros(NMAX,NP);

idx=ones(1,NP);

% for the prediction the particles make some random time iteration (to
% illustrate the fact that thay explore the state space at random. Here we
% simulate the noise that will be used for this predictio step.
noise_gauss = randn(NMAX,NP,NPRED);

% ------- technical graphic coefficient
vertical_correction = 0.005;
% -------

% +++++++ frame 0 (off the time loop) :
%         initial particles
clf
plot_ball(NP,ones(1,NP),position_cor(1:NP),zeros(1,NP),'g');
axis([XMIN XMAX YMIN YMAX ZMIN ZMAX]);
grid on;
view(-50,44);

xlabel('time');
ylabel('space');
plot_legend(0)
set(gcf,'DoubleBuffer','on')
drawnow
f_pause_plot
% ++++++ frame 0 (end)

for n=1:NMAX,

    % prediction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for k=1:NP,
        pck = position_cor(k);
        if n==1
            trajj = [0];
        else
            trajj = trajectory(idx(k),n-1,NPRED+1);
        end
        for npredic=1:NPRED
            pckk  = pck - ALPHA*DT*pck*(pck*pck-1) ...
                + SIGMAW*noise_gauss(n,k,npredic);
            pck   = pckk;
            trajj = [trajj pck];
        end
        trajectory(k,n,:) = trajj;
        position_pre(k)   = pckk;
    end
    weight_pre(1:NP) = weight_cor(1:NP);

    % +++++++ frame 1
    %         current paticles, past trajectories
    clf
    plot_ball(NP,n*NPRED*ones(1,NP),position_pre(1:NP),zeros(1,NP),'m');
    for nnn=1:n
        for k=1:NP
            traj(1:NPRED+1)=trajectory(k,nnn,1:NPRED+1);
            if nnn==1
                plot3((nnn-1)*NPRED+[1:NPRED],traj(2:NPRED+1),...
                    zeros(1,NPRED),'b')
            else
                plot3((nnn-1)*NPRED+[0:NPRED],traj,zeros(1,NPRED+1),'b')
            end
        end
    end
    hold on
    plot3([1:n*NPRED],x(1:n*NPRED),...
        vertical_correction+zeros(1,n*NPRED),'c');
    plot_legend(1)
    axis([XMIN XMAX YMIN YMAX ZMIN ZMAX]);grid on;view(-50,44);
    xlabel('time');
    ylabel('space');
    set(gcf,'DoubleBuffer','on')
    drawnow
    f_pause_plot
    % +++++++ frame 1 (end)

    % correction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for k=1:NP,
        if position_pre(k)<0
            hx = ALPHAM*position_pre(k);
        else
            hx = ALPHAP*position_pre(k);
        end
        weight_cor(k) = weight_pre(k)...
            *exp(-0.5*(y(n)-hx)*(y(n)-hx)/SIGMAV);
    end
    position_cor(1:NP) = position_pre(1:NP);
    weight_cor(1:NP)   = weight_cor(1:NP)/sum(weight_cor(1:NP));

    weightcor(n,1:NP)  = weight_cor;

    % +++++++ frame 2
    %         current particles
    %            + past trajectories (like frame 1)
    %            + likelihood function
    clf
    plot_ball(NP,n*NPRED*ones(1,NP),position_pre(1:NP),zeros(1,NP),'m');
    for nnn=1:n
        for k=1:NP
            traj(1:NPRED+1)=trajectory(k,nnn,1:NPRED+1);
            if nnn==1
                plot3((nnn-1)*NPRED+[1:NPRED],traj(2:NPRED+1),...
                    zeros(1,NPRED),'b')
            else
                plot3((nnn-1)*NPRED+[0:NPRED],traj,zeros(1,NPRED+1),'b')
            end
        end
    end
    hold on
    plot3([1:n*NPRED],x(1:n*NPRED),...
        vertical_correction+zeros(1,n*NPRED),'c')
    dplot=(YMAX-YMIN)/70.;lklhd=[];
    for dp=0:70,
        xval   = YMIN+dp*dplot;
        lklhd = [lklhd likelihood(xval,y(n))];
    end
    plot3(n*NPRED*ones(1,70+1),[YMIN:dplot:YMAX],lklhd,'r');
    plot_legend(2)
    axis([XMIN XMAX YMIN YMAX ZMIN ZMAX]);grid on;view(-50,44);
    xlabel('time');
    ylabel('space');
    set(gcf,'DoubleBuffer','on')
    drawnow
    % +++++++ frame 2 (end)

    % +++++++ frame 3
    %         current particles
    %            + past trajectories   (like frame 1)
    %            + likelihood function (like frame 2)
    %            + likelihood (vertical stems) associated with particles
    clf
    aaa = weight_cor(1)/likelihood(position_pre(1),y(n));
    weight_cor_aaa = (1/aaa)*weight_cor;
    gh  =stem3(n*NPRED*ones(1,NP),position_pre(1:NP),weight_cor_aaa(1:NP));
    set(gh,'Color',[1 0.6 0.6],'Marker','none');
    box off
    hold on
    for nnn=1:n
        for k=1:NP
            traj(1:NPRED+1)=trajectory(k,nnn,1:NPRED+1);
            if nnn==1
                plot3((nnn-1)*NPRED+[1:NPRED],traj(2:NPRED+1),...
                    zeros(1,NPRED),'b')
            else
                plot3((nnn-1)*NPRED+[0:NPRED],traj,zeros(1,NPRED+1),'b')
            end
        end
    end
    plot3(1:n*NPRED,x(1:n*NPRED),vertical_correction+zeros(1,n*NPRED),'c')
    plot_ball(NP,n*NPRED*ones(1,NP),position_pre(1:NP),zeros(1,NP),'m');

    dplot=(YMAX-YMIN)/70.;lklhd=[];
    for dp=0:70,
        xval   = YMIN+dp*dplot;
        lklhd = [lklhd  likelihood(xval,y(n))];
    end
    plot3(n*NPRED*ones(1,70+1),[YMIN:dplot:YMAX],lklhd,'r');
    plot_legend(3)
    axis([XMIN XMAX YMIN YMAX ZMIN ZMAX]);grid on;view(-50,44);
    xlabel('time');
    ylabel('space');
    set(gcf,'DoubleBuffer','on')
    drawnow
    f_pause_plot %%%%%%%
    % +++++++ frame 3 (end)

    % resampling %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    idxx=resampling_rand(weight_cor);

    position_cor=[];
    idx=[];
    for nnn=1:NP
        for iii=1:idxx(nnn)
            position_cor = [position_cor position_pre(nnn)];
            idx =          [idx          nnn];
        end
    end
    weight_cor=ones(1,NP)/NP;

    % +++++++ frame 4
    %         current particles
    %            + past trajectories   (like frame 1)
    %            + likelihood function (like frame 2)
    %            + likelihood (vertical stems) associated
    %               with particles     (like frame 3)
    %            + "boulier" of the new particles
    clf
    gh=stem3(n*NPRED*ones(1,NP),position_pre(1:NP),weight_cor_aaa(1:NP));
    set(gh,'Color',[1 0.6 0.6],'Marker','none');
    box off
    hold on
    for nnn=1:n
        for k=1:NP
            traj(1:NPRED+1)=trajectory(k,nnn,1:NPRED+1);
            if nnn==1
                plot3((nnn-1)*NPRED+[1:NPRED],traj(2:NPRED+1),...
                    zeros(1,NPRED),'b')
            else
                plot3((nnn-1)*NPRED+[0:NPRED],traj,zeros(1,NPRED+1),'b')
            end
        end
    end
    hold on
    plot3([1:n*NPRED],x(1:n*NPRED),vertical_correction+zeros(1,n*NPRED),'c')
    plot_ball(NP,n*NPRED*ones(1,NP),position_pre(1:NP),zeros(1,NP),'m');

    dplot=(YMAX-YMIN)/70.;lklhd=[];
    for dp=0:70,
        xval   = YMIN+dp*dplot;
        lklhd = [lklhd  likelihood(xval,y(n))];
    end
    plot3(n*NPRED*ones(1,70+1),[YMIN:dplot:YMAX],lklhd,'r'); 
    axis([XMIN XMAX YMIN YMAX ZMIN ZMAX]);grid on;view(-50,44);
    xlabel('time');
    ylabel('space');
    plot_legend(4)
    plot_resampling(n*NPRED,NP,position_cor);
    set(gcf,'DoubleBuffer','on')
    drawnow
    f_pause_plot %%%%%%%
    % +++++++ frame 4 (end)

    % +++++++ frame 5
    %         current particles
    %            + past trajectories   (like frame 1)
    clf
    plot_ball(NP,n*NPRED*ones(1,NP),position_cor(1:NP),zeros(1,NP),'g');
    for nnn=1:n
        for k=1:NP
            traj(1:NPRED+1)=trajectory(k,nnn,1:NPRED+1);
            if nnn==1
                plot3((nnn-1)*NPRED+[1:NPRED],traj(2:NPRED+1),...
                    zeros(1,NPRED),'b')
            else
                plot3((nnn-1)*NPRED+[0:NPRED],traj,zeros(1,NPRED+1),'b')
            end
        end
    end
    hold on
    plot3(1:n*NPRED,x(1:n*NPRED),vertical_correction+zeros(1,n*NPRED),'c')
    plot_legend(5)
    axis([XMIN XMAX YMIN YMAX ZMIN ZMAX]);grid on;view(-50,44);
    xlabel('time');
    ylabel('space');
    set(gcf,'DoubleBuffer','on')
    drawnow
    f_pause_plot %%%%%%%
    % +++++++ frame 5 (end)


    positions(n,1:NP) = position_cor;
    weights(n,1:NP)     = weight_cor;
    indices(n,1:NP)   = idx;

end


function v=likelihood(x,y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the likelihood weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global NMAX NPRED ALPHA ALPHAM ALPHAP DT XINI VINI SIGMA2W SIGMA2V...
    SEEDIN NP SIGMAV SIGMAW DDT XMIN XMAX YMIN YMAX ZMIN ...
    ZMAX
if x<0
    hx = ALPHAM*x;
else
    hx = ALPHAP*x;
end
v=exp(-0.5*(y-hx)*(y-hx)/SIGMAV);


function plot_resampling(n,np,position)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot the resampling step
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xval    = zeros(1,np);
pval    = zeros(1,np);
xval(1) = position(1);
pval(1) = 1;
nnn     = 1;
for i=2:np
    if ismember(position(i),xval(1:i-1))
        indd       = find(position(i)*ones(1,i-1)==xval(1:i-1));
        pval(indd) = pval(indd) +1;
    else
        nnn       = nnn+1;
        xval(nnn) = position(i);
        pval(nnn) = 1;
    end
end
[xval(1:nnn),idx] = sort(xval(1:nnn));
pval(1:nnn)       = pval(idx);
for i=1:nnn
    plot_ball(pval(i),n*ones(1,pval(i)),xval(i)*ones(1,pval(i)),...
        0.001+0.07*[1:pval(i)],[0.01 0.3 0.01]);
    hold on
end


function plot_legend(i)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot the legends prediction/correction/selection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global NMAX NPRED ALPHA ALPHAM ALPHAP DT XINI VINI SIGMA2W SIGMA2V...
    SEEDIN NP SIGMAV SIGMAW DDT XMIN XMAX YMIN YMAX ZMIN ...
    ZMAX
xshift = 15;
yshift = YMAX-0.2;
zshift = 0.7;
switch i
    case 0 % prediction
        text(xshift,yshift,zshift+0.80,'prediction',...
            'FontSize',10,'Color','m')
        text(xshift,yshift,zshift+0.73,'correction',...
            'FontSize',10,'Color','r')
        text(xshift,yshift,zshift+0.66,'selection',...
            'FontSize',10,'Color','g')
    case 1 % prediction
        text(xshift,yshift,zshift+0.80,'\rightarrow ',...
            'FontSize',10,'Color','r','HorizontalAlignment','right')
        text(xshift,yshift,zshift+0.80,...
            'prediction','FontSize',10,'Color','m')
        text(xshift,yshift,zshift+0.73,...
            'correction','FontSize',10,'Color','r')
        text(xshift,yshift,zshift+0.66,...
            'selection', 'FontSize',10,'Color','g')
    case 2 % correction (likelihood)
        text(xshift,yshift,zshift+0.73,'\rightarrow ',...
            'FontSize',10,'Color','r','HorizontalAlignment','right')
        text(xshift,yshift,zshift+0.80,...
            'prediction','FontSize',10,'Color','m')
        text(xshift,yshift,zshift+0.73,...
            'correction','FontSize',10,'Color','r')
        text(xshift,yshift,zshift+0.66,...
            'selection', 'FontSize',10,'Color','g')
    case 3 % correction (new weigths)
        text(xshift,yshift,zshift+0.73,'\rightarrow ',...
            'FontSize',10,'Color','r','HorizontalAlignment','right')
        text(xshift,yshift,zshift+0.80,...
            'prediction','FontSize',10,'Color','m')
        text(xshift,yshift,zshift+0.73,...
            'correction','FontSize',10,'Color','r')
        text(xshift,yshift,zshift+0.66,...
            'selection', 'FontSize',10,'Color','g')
    case 4 % selection
        text(xshift,yshift,zshift+0.66,'\rightarrow ',...
            'FontSize',10,'Color','r','HorizontalAlignment','right')
        text(xshift,yshift,zshift+0.80,...
            'prediction','FontSize',10,'Color','m')
        text(xshift,yshift,zshift+0.73,...
            'correction','FontSize',10,'Color','r')
        text(xshift,yshift,zshift+0.66,...
            'selection', 'FontSize',10,'Color','g')
    case 5 % selection
        text(xshift,yshift,zshift+0.66,'\rightarrow ',...
            'FontSize',10,'Color','r','HorizontalAlignment','right')
        text(xshift,yshift,zshift+0.80,...
            'prediction','FontSize',10,'Color','m')
        text(xshift,yshift,zshift+0.73,...
            'correction','FontSize',10,'Color','r')
        text(xshift,yshift,zshift+0.66,...
            'selection','FontSize',10,'Color','g')
end

function plot_ball(nballs,x0,y0,z0,color)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot the ... balls
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global NMAX NPRED ALPHA ALPHAM ALPHAP DT XINI VINI SIGMA2W SIGMA2V...
    SEEDIN NP SIGMAV SIGMAW DDT XMIN XMAX YMIN YMAX ZMIN ...
    ZMAX
scale   = 0.004;
[X,Y,Z] = sphere(20);
xs = (XMAX-XMIN)*scale*3; % x scale;
ys = (YMAX-YMIN)*scale*3; % y scale;
zs = (ZMAX-ZMIN)*scale*5; % z scale;
for nb=1:nballs
    surf(x0(nb)+xs*X,y0(nb)+ys*Y,z0(nb)+zs*Z,'FaceColor',color,...
        'LineStyle','none','EdgeColor','k')
    camlight(140,180);
    lighting phong;
    hold on
end


function indices=resampling_rand(weights)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function indices=resampling_rand(weights)
%     weights  probability law
%     indices  indices of the selected points
% (indices(k)=j means that the point k is resampled j times)
% resampling according the discrete law weights(k) k=1:np
% with  np=length(weights)
%     1) we compute the integer part of np*weights
%     2) we sample the remaining particles according to the "remaining"
%        weights
%           weights(k)-floor(weights(k)*np)/np   k=1:np
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
np       = length(weights);
% --- computation of the np first particles
indices  = floor(np*weights);
weights  = weights - (1/np)*indices;
% --- np-sum(indices) particles remain to be selected
nnp      = np - sum(indices);
if nnp > 0
    weights = weights/sum(weights);
    weights = cumsum(weights);
    for k=1:nnp
        threshold = rand;
        i = 1;
        while weights(i)<threshold,
            i=i+1;
        end
        indices(i) = indices(i)+1;
    end
end

