%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function demo_map_matching()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (C) Fabien Campillo - INRIA
%--------------------------------------------------------------------------
% aircraft positioning by map matching (terrain elevation model)
%--------------------------------------------------------------------------
% created on 25 April 2003 - modified on January 16, 2009
% - Jan 16, 2009 : cleaning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('  ');
disp('  ');
disp('---- demo: particle filter with map related measurements ');
disp('           (digital elevation model)                     ');
disp('  ');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loading the map
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('   >>> loading the map...');
map  = load('demo_map_matching_map.data');
[N1 N2] = size(map);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- position domain

X1MIN = -10000;
X1MAX =  10000;
X2MIN = -10000;
X2MAX =  10000;

% --- observation

TIME_NB  = 100 ;  % number of observations
OBS_SD   =  7;    % standard deviation of the observation noise

% --- mobile trajectory parameters (constant speed and heading)

TRAJ_X01 = -6000 ;
TRAJ_X02 = -4000 ;
TRAJ_SP  = 120. ;
TRAJ_HD  = 0;
VAR_SP   = 15;
VAR_HD   = 10;

% --- filter parameters

PARTICLES_NB = 4000;

% --- initial law (Gaussian)

ESP0_X1 = -6000-1000 ; % mean for rx
ESP0_X2 = -6000+1000 ; % mean for ry
ESP0_HD = 5;           % mean for heading
ESP0_SP = 140 ;        % mean for speed
VAR0_X1 = 6000000 ;    % variance for rx
VAR0_X2 = 6000000  ;   % variance for ry
VAR0_HD = 3;           % variance for heading
VAR0_SP = 6 ;          % variance for vitesse

% --- random number generator initialization

SEEDIN = 990;
randn('state',SEEDIN);

% --- cosmetic

particles_col  = 'blue';
particles_size = 1;
traj_col       = 'red';
traj_size      = 10;
mean_col       = 'green';
mean_size      = 10;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEP 1: the set up
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- plotting the terrain ------------------------------------------------

clf;
imagesc([X1MIN X1MAX],[X2MIN X2MAX],transpose(map));
hold on;
colormap('gray');
brighten(-0.5);
brighten(-0.5);
axis square;
axis off;

% --- une petite analyse du terrain ---------------------------------------
answer_terrain = input('\n   >>> terrain analysis ? y/n [n] ','s');

if isempty(answer_terrain)
  answer_terrain = 'n'; 
end

if answer_terrain=='y'

  [ii,jj,mmap] = find(map+9999); % mmap is all the elements of map
  mmap=mmap-9999;                % different from -9999

  fprintf(['        statistics (out of the -9999) \n']);
  fprintf(['           mean ' num2str(mean(mmap)) ' \n']);
  fprintf(['           standard deviation ' num2str(std(mmap)) ' \n']);
  fprintf(['           minimum ' num2str(min(mmap)) ' \n']);
  fprintf(['           maximum ' num2str(max(mmap)) ' \n']);
  fprintf(['           (' num2str(length(mmap)) ' sites over ' ...
           num2str(N1*N2) ') \n']);

  fprintf('       ... pause [return to continue] ') ;
  pause
  fprintf('     \n') ;
  
end

% --- brighten or darken the current colormap -----------------------------
answer_lum = input('\n   >>> set the brightness ? y/n [n] ','s');
if isempty(answer_lum),  answer_lum = 'n'; , end

if answer_lum == 'y'
  while 1
    answer_lum2 = ...
        input('          plus (p) minus (m) ok (o) p/m/o [o] : ','s');
    if isempty(answer_lum2), answer_lum2='o'; end
    if answer_lum2=='o', break; end
    % brighten or darken the current colormap:
    if answer_lum2=='p',   brighten( 0.5); drawnow; end
    if answer_lum2=='m',   brighten(-0.5); drawnow; end
  end
end

% --- dynamics of the particles -----------------------------------------------

answer_dynamique = input('\n   >>> particles dynamics ? y/n [n] ','s');
if isempty(answer_dynamique)
  answer_dynamique = 'n';
end

if answer_dynamique=='y'

  % --- initial co-ordinates
  
  x1      = 0.5 * (X1MIN+X1MAX) * ones(1,PARTICLES_NB) ;
  x2      = 0.5 * (X2MIN+X2MAX) * ones(1,PARTICLES_NB) ;
  speed   = TRAJ_SP * ones(1,PARTICLES_NB);
  heading = TRAJ_HD * ones(1,PARTICLES_NB);
  
  % --- graph
  
  clf;
  plot(x1(1:PARTICLES_NB),x2(1:PARTICLES_NB),...
       'LineStyle','none','MarkerSize',particles_size,...
       'Marker','p','Color',particles_col);
  hold on;
  axis([X1MIN X1MAX X2MIN X2MAX]);
  axis off;
  
  % --- iterations
  
  set(gcf,'DoubleBuffer','on')
  
  for t=1:40
    
    speed   = speed   + VAR_SP  * randn(1,PARTICLES_NB);
    heading = heading + VAR_HD  * randn(1,PARTICLES_NB);
    x1      = x1      + speed  .* cos((pi/180.)*heading);
    x2      = x2      + speed  .* sin((pi/180.)*heading);
    
    clf;
    plot(x1(1:PARTICLES_NB),x2(1:PARTICLES_NB),...
         'LineStyle','none','MarkerSize',particles_size,...
         'Marker','p','Color',particles_col);
    hold on;
    axis([X1MIN X1MAX X2MIN X2MAX]);
    axis square;
    drawnow;
    
  end
  
  fprintf('       ... pause [return to continue] ') ;
  pause
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEP 2: simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- trajectory (here a straight line)

TRAJ_X1 = TRAJ_X01 + [0:TIME_NB]*TRAJ_SP*cos(TRAJ_HD*pi/180.);
TRAJ_X2 = TRAJ_X02 + [0:TIME_NB]*TRAJ_SP*sin(TRAJ_HD*pi/180.);

% --- observation (noisy profile)

the_profile = zeros(1,TIME_NB);
for t=1:TIME_NB
  h = altitude(TRAJ_X1(t),TRAJ_X2(t),N1,N2,X1MIN,X1MAX,X2MIN,X2MAX,map);
  the_profile(t) = h;
end

observation = the_profile + OBS_SD * randn(1,TIME_NB);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEP 3: filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- initialisation ------------------------------------------------------

% initialisation of the positions
%   1st possibility: Gaussian law
x1 = ESP0_X1 + sqrt(VAR0_X1) * randn(1,PARTICLES_NB) ;
x2 = ESP0_X2 + sqrt(VAR0_X2) * randn(1,PARTICLES_NB) ;
%   2nd posssibility: Uniform law
%       x1 = X1MIN + (X1MAX-X1MIN) * rand(1,PARTICLES_NB) ;
%       x2 = X2MIN + (X2MAX-X2MIN) * rand(1,PARTICLES_NB) ;


% heading initialization
%   1st possibility : Gaussian law
heading = ESP0_HD + sqrt(VAR0_HD) * randn(1,PARTICLES_NB) ;
%   2nd possibility : uniform law over [0,360]
%       heading = 360 * rand(1,PARTICLES_NB) ;

% speed initialization
speed = ESP0_SP + sqrt(VAR0_SP) * randn(1,PARTICLES_NB) ;

% --- graph ---------------------------------------------------------------

clf;
imagesc([X1MIN X1MAX],[X2MIN X2MAX],transpose(map));
hold on;
axis square;
axis off;

fprintf('\n   >>> initial state ') ;
fprintf('\n       + the terrain [return to continue] ') ;
pause

plot(TRAJ_X1(1),TRAJ_X2(1),...
     'LineStyle','none','MarkerSize',traj_size,...
     'Marker','.','Color',traj_col);
plot(TRAJ_X1,TRAJ_X2,...
     'LineStyle','none','MarkerSize',1,'Marker','p','Color',traj_col);

fprintf(['\n       + mobile trajectory (point : initial position)']) ;
fprintf(['\n         [return to continue] ']) ;
pause

plot(x1(1:PARTICLES_NB),x2(1:PARTICLES_NB),...
     'LineStyle','none','MarkerSize',particles_size,...
     'Marker','p','Color',particles_col);
fprintf('\n       + initial particles [return to continue] ') ;
fprintf('\n') ;

% --- iterations ----------------------------------------------------------

% --- memory allocation

x1esp = zeros(1,TIME_NB);
x2esp = zeros(1,TIME_NB);

% ---

x1esp(1) = mean(x1);
x2esp(1) = mean(x2);

fprintf('\n   >>> initial state [return to continue] ') ;
clf;
imagesc([X1MIN X1MAX],[X2MIN X2MAX],transpose(map));
hold on;
axis square;
axis off;
plot(x1(1:PARTICLES_NB),x2(1:PARTICLES_NB),...
     'LineStyle','none','MarkerSize',particles_size,...
     'Marker','p','Color',particles_col);
plot(TRAJ_X1(1),TRAJ_X2(1),...
     'LineStyle','none','MarkerSize',traj_size,...
     'Marker','.','Color',traj_col);
% plot(x1esp(1),x2esp(1),...
%     'LineStyle','none','MarkerSize',mean_size,...
%     'Marker','.','Color',mean_col);

pause
fprintf('\n\n   >>> iterations...  ') ;
% F(1) = getframe;

set(gcf,'DoubleBuffer','on')

for t=1:TIME_NB
  
  % --- prediction ------------------------------------------------------
  
  speed   = speed + VAR_SP * randn(1,PARTICLES_NB);
  heading = heading     + VAR_HD     * randn(1,PARTICLES_NB);
  x1      = x1 + speed .* cos((pi/180.)*heading);
  x2      = x2 + speed .* sin((pi/180.)*heading);
  
  % --- correction ------------------------------------------------------
  
  % --- likelihood
  
  h(1:PARTICLES_NB) = ...
      altitude(x1(:),x2(:),N1,N2,X1MIN,X1MAX,X2MIN,X2MAX,map);
  omega(1:PARTICLES_NB) = (observation(t)-h(:)) .* (observation(t) -h(:));
  omega = exp(-0.5 * omega / (OBS_SD*OBS_SD) );
  
  % normalisation
  the_sum = sum(omega);
  if the_sum == 0
    fprintf('\n        WARNING > null likelihood ');
    omega = ones(1,PARTICLES_NB) / PARTICLES_NB;
  else
    omega = omega / the_sum;
  end
  
  % --- selection
  
  %  offsprings = f_resample_comb_randshift(omega);
  offsprings = f_resample_residual(omega);
  indices    = f_resample_indices(offsprings);
  
  x1      = x1(indices);
  x2      = x2(indices);
  heading = heading(indices) ;
  speed   = speed(indices) ;
  
  % --- conditionnal mean
  
  x1esp(t) = mean(x1);
  x2esp(t) = mean(x2);
  
  % --- graph
  
  clf;
  imagesc([X1MIN X1MAX],[X2MIN X2MAX],transpose(map));
  hold on;
  axis square;
  axis off;
  plot(x1(1:PARTICLES_NB),x2(1:PARTICLES_NB),...
       'LineStyle','none','MarkerSize',particles_size,...
       'Marker','p','Color',particles_col);
  plot(TRAJ_X1(t),TRAJ_X2(t),...
       'LineStyle','none','MarkerSize',traj_size,...
       'Marker','.','Color',traj_col);
  % plot(x1esp(t),x2esp(t),...
  %     'LineStyle','none','MarkerSize',mean_size,...
  %     'Marker','.','Color',mean_col);
  drawnow
  
  %  F(t+1) = getframe;
  
end

fprintf('\n \n   >>> bye \n \n ') ;

% --- avi output ----------------------------------------------------------
% answer_avi = input('\n\n   >>> avi output (slow...) ? y/n [n] ','s');
% if isempty(answer_avi), answer_avi = 'n'; end
% if answer_avi=='y'


%   fprintf('\n\n   >>> creation of the movie...\n') ;
%   movie(F,0);
%   movie2avi(F,strcat(input('       enter the file name : ','s'),'.avi'));

% end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function h=altitude(x1,x2,n1,n2,x1min,x1max,x2min,x2max,map)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function h=altitude(x1,x2)
%     x1 x2    position du mobile
%     n1 n2
%     x1min x1max x2min x2max
%              zone de map
%     map      de taille n1 x n2
%     h        altitude du mobile en position x1 x2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h= -9999 * ones(size(x1));

indices = (x1min<x1) & (x1<x1max) & (x2min<x2) & (x2<x2max) ;

i1 = floor(n1 * (x1-x1min)/(x1max-x1min));
i2 = floor(n2 * (x2-x2min)/(x2max-x2min));

i1 = min(max(i1,1),n1);
i2 = min(max(i2,1),n2);

% hum... le sub2ind permet d'obtenir un array de la bonne
% taille.... hh doit etre de size 1 x N (et pas N x N)
hh  = map(sub2ind(size(map),i1,i2));
h(indices)  = hh(indices);

