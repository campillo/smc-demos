%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================




% >>> SIMULATION PARAMETERS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


n_px       = 120;         %> number of pixels in x
n_py       = 120;         %> number of pixels in y
n_p        = n_px*n_py;
nb_frames  = 50;         %> number of pics in the pic sequence
frames_per_seconde = 10;  %> number of pics per secondes

rx_min =   -100;         %> physical map (spatial coordinates)
rx_max =    100;         %>           "
ry_min =   -100;         %>           "
ry_max =    100;         %>           "
vx_min = -10; % the initial law is uniform 
vx_max =  10; % in this domain (for r : 0,0)
vy_min = -10; %
vy_max =  10; %
ax_min = -5;  %
ax_max =  5;  %
ay_min = -5;  %
ay_max =  5;  %

snr = 1;        %> signal-to-noise ratio in decibels % to check

% --- derived parameters

total_time = nb_frames/frames_per_seconde;
delta_t    = 1/frames_per_seconde;
delta_rx   = (rx_max-rx_min)/n_px;
delta_ry   = (ry_max-ry_min)/n_py;
coord_x    = rx_min+delta_rx*[1:n_px]-delta_rx/2;
coord_y    = ry_min+delta_ry*[1:n_py]-delta_ry/2;
% the intensity of the mobile is given (=1) and the NSR, we deduce
% the intensity of the observation noise
intensity  = sqrt(10^(-0.1*snr)) ; % the nice NSR formula in dB



