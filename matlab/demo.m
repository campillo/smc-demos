%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================




%--------------------------------------------------------------------------
% main routine
%--------------------------------------------------------------------------

disp('==================================================================')
disp('SMC demos [Sequential Monte Carlo demos] V1.0                     ')
disp('------------------------------------------------------------------')
disp('(C) Fabien Campillo                                               ')
disp('    Fabien.Campillo@inria.fr                                      ')
disp('    INRIA                                                         ')
disp('    version 1.0 - March 2009                                      ')
disp('==================================================================')

while 1 %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
    fprintf(['    \n' ...
        ' ------------------ \n' ...
        ' >>> pick up a demo \n' ...
        '       1. demo 1D \n' ...
        '       2. demo 1D without redistribution \n' ...
        '       3. altimetric matching with digital elevation model \n'...
        '       4. tracking with obstacles \n' ...
        '       5. tracking with obstacles (step by step)\n' ...
        '       6. tracking in pictures sequence \n' ...
        '       7. geolocation \n' ...
        '       8. geolocation (step by step) \n' ...
        '       q. quit \n']);

    answer=input('     choice: ','s');

    %  if isempty(answer),  answer = '1'; , end
  
    switch answer

        case '1'
            demo_1D_res
        case '2'
            demo_1D_nores
        case '3'
            demo_map_matching
        case '4'
            demo_tracking
        case '5'
            demo_tracking_step
        case '6'
            demo_pixel_tracking_RB
        case '7'
            demo_geoloc
        case '8'
            demo_geoloc_step
        case 'q'
            break

    end

end
