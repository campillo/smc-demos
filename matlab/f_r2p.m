%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function [px,py]=f_r2p(rx,ry,rx_min,ry_min,delta_rx,delta_ry)
%------------------------------------------------------------------------------
% transform the spatiales coordinates into real coordinates
%------------------------------------------------------------------------------
px = round((rx-rx_min)./delta_rx+0.5);
py = round((ry-ry_min)./delta_ry+0.5);


