%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 1, 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%--------------------------------------------------------------------------

This folder contains a set of demonstration matlab procedures for
nonlinear filtering approximation via particle filtering (sequential
Monte Carlo), just run:

  >> demo

Some procedures are simple, other are rather difficult and sometimes
in French (sorry for that). The next version will be improved... Anyway
enjoy it.

The demo demo_pixel_tracking_RB is tricky. The complete run is
    	 If you want another simulation, you should run:
	    	>> demo_pixel_tracking_simu
	then:
		>> demo_pixel_tracking_RB
		Here the simulation is done and the demo runs the 
		demo_pixel_tracking_RB procedure


Some special files:
  - demo_map_matching_map.data: used by demo_map_matching (don't
     destroy it !)
  - demo_geoloc.mat : generated by demo_geoloc (and _step version)
  - demo_pixel_tracking_workspace.mat: generated by demo_pixel_tracking_RB

the 2 last files are generated by the demo files.

DOCUMENTATION : for now, the only available documentation is in French
see:

	ftp://ftp-sop.inria.fr/modemic/campillo/cours/2007-master2-toulon.pdf

Number of source lines code : 
       6128 (including comments and blank lines)
       3157 (matlab code lines)


Fabien Campillo
10 March 2009
[revised 28 June 2012]
