%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================



function [the_map_x,the_map_y] = f_comp_map(weights,px,py)
%------------------------------------------------------------------------------

the_map_x = [];
the_map_y = [];

for n=1:length(weights(:,1))

  [max_weights,imax_weights]=max(weights(n,:));

  the_map_x = [the_map_x px(n,imax_weights)];
  the_map_y = [the_map_y py(n,imax_weights)];

end
