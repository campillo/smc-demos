%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function offsprings=f_resample_comb_randperm(weights)
% function offsprings=f_resample_comb(weights)
% the comb algo with randperm
  n_weights     = length(weights);
  permut        = randperm(n_weights);
  weights       = weights(permut);
  weights       = cumsum(weights);
  weights       = weights/weights(end);
  weights       = floor(weights*(n_weights));
  offsprings    = [weights(1) weights(2:n_weights)-weights(1:n_weights-1)];
  offsprings(permut) = offsprings;
% =========================================================================
% Author: Fabien Campillo Fabien.Campillo@inria.fr 
% This source code is freely distributed for educational, research and 
% non-profit purposes. Permission to use it in commercial products may 
% be obtained from the author.
% =========================================================================

