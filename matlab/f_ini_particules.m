%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================



function [prx,pry,pvx,pvy,pax,pay,ini_zone] = ...
    f_ini_particules(n,inirx,inivx,iniax,t_rx,t_ry,t_vx,t_vy,t_ax,t_ay)
%-------------------------------------------------------------------------------
% initialization of the particles
% uniform laws with the parameters
%    ini r x min  
%        v y max
%        a
% so 1x3x2x2=12 parameters
% for example for the position r in x, we have two parameters
%    U[ ini_rx_min ini_rx_max]
% comprendo ?
%-------------------------------------------------------------------------------

% we take cool initial condition : + or - centered on the real 
% values with a given biais

iniry = inirx ;
inivy = inivx ;
iniay = iniax ;

% --- not really centered...

rrx = rand;
rry = rand;
inirx_min = t_rx + rrx     * inirx ;
inirx_max = t_rx + (rrx-1) * inirx ;
iniry_min = t_ry + rry     * iniry ;
iniry_max = t_ry + (rry-1) * iniry ;

rrx = rand;
rry = rand;
inivx_min = t_vx + rrx     * inivx ;
inivx_max = t_vx + (rrx-1) * inivx ;
inivy_min = t_vy + rry     * inivy ;
inivy_max = t_vy + (rry-1) * inivy ;

rrx = rand;
rry = rand;
iniax_min = t_ax + rrx     * iniax ;
iniax_max = t_ax + (rrx-1) * iniax ;
iniay_min = t_ay + rry     * iniay ;
iniay_max = t_ay + (rry-1) * iniay ;

ini_zone  = [inirx_min inirx_max iniry_min iniry_max...
	     inivx_min inivx_max inivy_min inivy_max...
	     iniax_min iniax_max iniay_min iniay_max];

% --- particle initisalization

prx = inirx_min + (inirx_max-inirx_min)*rand(1,n);
pry = iniry_min + (iniry_max-iniry_min)*rand(1,n);
pvx = inivx_min + (inivx_max-inivx_min)*rand(1,n);
pvy = inivy_min + (inivy_max-inivy_min)*rand(1,n);
pax = iniax_min + (iniax_max-iniax_min)*rand(1,n);
pay = iniay_min + (iniay_max-iniay_min)*rand(1,n);
