%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function f_plot_ellipse(fmean,fcov,npoint,fcolor,R)
% function f_plot_ellipse(fmean,fcov,npoint,fcolor)
% trace a "90%" ellipse correspondent with the two-dimensional 
% normal law N(fmean, fcov).
% parameters:  npoint : number of points for the layout of the ellipse 
%              color [character] color of the ellipse
% one decomposes the covariance matrix R = P D P' 
% (P orthonormal D diagonal) then Z = D^{-1/2 } P (x-m) 
% where M is the average, then |Z|^2 = R the parametric equation 
% of the ellipse is 
%      y(s) = m + P' sqrt(r D) [ cos(s) sin(s) ]'
% |Z|^2 follows a law of the chi2 with two degrees of freedom 
% R=4.61 gives an ellipse of confidence of 90%
if nargin<=4, R=4.61; end     % default R=4.61 gives an ellipse of 
                              % confidence of 90%
if nargin<=3, fcolor='k'; end % default fcolor is 'black'
if nargin<=2, npoint=45; end  % default 45 points

[vec,val] = eig(fcov);
t         = linspace(0,2*pi,npoint);
ellipse   = vec * sqrt(val*R) * [cos(t);sin(t)];
ellipse   = ellipse + repmat([fmean(1);fmean(2)],1,npoint);
plot(ellipse(1,:),ellipse(2,:),fcolor)

