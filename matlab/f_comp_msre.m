%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function msre = f_comp_msre(weights,px,py)
%------------------------------------------------------------------------------

meanx = [];
meany = [];

for n=1:length(weights(:,1))

  meanx = [meanx sum(weights(n,:).* px(n,:)) ];
  meany = [meany sum(weights(n,:).* py(n,:)) ];

end

msre= [];

for n=1:length(weights(:,1))
  
  msre= [msre ...
	 sum(weights(n,:) .* ((px(n,:)-meanx(n)).^2 + (py(n,:)-meany(n)).^2))];

end

;
