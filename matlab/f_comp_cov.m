%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================



function [covxx,covxy,covyy] = f_comp_cov(weights,px,py)
%------------------------------------------------------------------------------

meanx = [];
meany = [];
covxx = [];
covxy = [];
covyy = [];

for n=1:length(weights(:,1))
  
  meanx = [meanx sum(weights(n,:).* px(n,:)) ];
  meany = [meany sum(weights(n,:).* py(n,:)) ];
  
  covxx = [covxx sum(weights(n,:).* px(n,:) .* px(n,:))];
  covxy = [covxy sum(weights(n,:).* px(n,:) .* py(n,:))];
  covyy = [covyy sum(weights(n,:).* py(n,:) .* py(n,:))];

end

covxx = covxx - meanx .* meanx;
covxy = covxy - meanx .* meany;
covyy = covyy - meany .* meany;

