%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================



function [rx,ry]=f_p2r(px,py,rx_min,ry_min,delta_rx,delta_ry)
%------------------------------------------------------------------------------
% transform pixel coordinates to spatial coordinates
%------------------------------------------------------------------------------
   rx = rx_min+delta_rx*(px-0.5);
   ry = ry_min+delta_ry*(py-0.5);

