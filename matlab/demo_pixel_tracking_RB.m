%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function demo_pixel_tracking_RB()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RB + vectorized
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('demo_pixel_tracking_workspace.mat')
demo_pixel_tracking_parameters_simu
demo_pixel_tracking_parameters_filter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART 4 - filtering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp(' ');
disp(' --- tracking in a sequence of pics ------------------------------');
disp(' ');
disp('      If you want another simulation, you should run:');
disp('            >> demo_pixel_tracking_simu');
disp('      then:');
disp('            >> demo_pixel_tracking_RB');
disp('      Here the simulation is done and the demo runs the ');
disp('      demo_pixel_tracking_RB procedure');
disp('   ');
disp(' >>> filtering ');

% notation :
%    pri(j,k,l) particle
%       i=x or y (coordinated x or y)
%       j=1:nb_frames, time index
%       k=1 or 2, 1 for the predicted points, 2 for the corrected points
%       l=1:nbp, indice for the particle
% algorithm :
%     predicted initial particles  p..(1,1,.) 
%     for n=1:nb_frames
%       computed likelihood                  L(p..(n,1,.))
%       selection of the corrected particles p..(n,2,.)
%       predicted particles                  p..(n+1,1,.)
%     end


% --- initialization ---------------------------------------------------------
%     of the particles computation of p..(1,1,.) [instant 1 - prediction]
%     of the mean of the RB Kalman filters

[prx(1,1,:),pry(1,1,:),pvx(1,1,:),pvy(1,1,:),pax(1,1,:),pay(1,1,:),ini_zone] ...
    = f_ini_particules(nbp,ini_rx_ecart,ini_vx_ecart,ini_ax_ecart,...
		     t_rx(1),t_ry(1),t_vx(1),t_vy(1),...
		     t_ax(1),t_ay(1));

alpha_x = [squeeze(pax(1,1,:))' ; 
           squeeze(pvx(1,1,:))'];
alpha_y = [squeeze(pay(1,1,:))' ; 
           squeeze(pvy(1,1,:))'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iterations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h_bar2 = waitbar(0,'Filtering, please wait...');
tic

for n=1:nb_frames
  
  waitbar(n/nb_frames)
  
  i_observed(:,:) = squeeze(observed_sequence(n,:,:));
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % step 1 : likelihood computation
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % --- we transform the observed picture to an unidim array
  %     (from i_observed to i_vois and i_masq)

  [i_vois,i_masq] = f_i_trans(i_observed,Npsf);

  rx = squeeze(prx(n,1,:));
  ry = squeeze(pry(n,1,:));
  px = round((rx-rx_min)./delta_rx+0.5);
  py = round((ry-ry_min)./delta_ry+0.5);

  % the non out-of-range
  noor = (1<=px) & (px<=n_px) & (1<=py) & (py<=n_py);
  ind_p = zeros(1,nbp);
  if ~isempty(px(noor))
    ind_p(noor) = sub2ind([n_px n_py],px(noor),py(noor));
  end

  rrx = rx_min+delta_rx*(px-0.5);
  rry = ry_min+delta_ry*(py-0.5);

  % xx and yy should be switch...!
  % [xx,yy] = meshgrid([-Npsf:+Npsf],[-Npsf:+Npsf]);
  % [yy,xx] = meshgrid([-Npsf:+Npsf],[-Npsf:+Npsf]);
  % xx = (reshape(xx,1,NNpsf));
  % yy = (reshape(yy,1,NNpsf));
  % xxx = ones(nbp,NNpsf);
  % yyy = ones(nbp,NNpsf);
  % for l=1:NNpsf
  %   xxx(:,l) = delta_rx*xx(l)-(rx(:)-rrx(:));
  %   yyy(:,l) = delta_ry*yy(l)-(ry(:)-rry(:));
  % end
  % xxx = fliplr(xxx);
  % yyy = fliplr(yyy);
  % psf = -0.5/sigma2psf*(xxx.*xxx + yyy.*yyy);
  % psf = psf-repmat(psf(1:nbp,floor(NNpsf/2)+1),1,NNpsf);
  % psf = exp(psf);

  [xx,yy] = meshgrid([-Npsf:+Npsf],[-Npsf:+Npsf]);
  xx = (reshape(xx,1,NNpsf));
  yy = (reshape(yy,1,NNpsf));
  xxx = ones(nbp,NNpsf);
  yyy = ones(nbp,NNpsf);
  for l=1:NNpsf
    xxx(:,l) = delta_rx*xx(l)-(rx(:)-rrx(:));
    yyy(:,l) = delta_ry*yy(l)-(ry(:)-rry(:));
  end
  % xxx = fliplr(xxx);
  % yyy = fliplr(yyy);
  psf = -0.5/sigma2psf*(xxx.*xxx + yyy.*yyy);
  psf = psf-repmat(psf(1:nbp,floor(NNpsf/2)+1),1,NNpsf);
  psf = exp(psf);

  vois = zeros(nbp,NNpsf);
  masq = zeros(nbp,NNpsf);
  vois(noor,1:NNpsf) = i_vois(ind_p(noor),1:NNpsf);
  masq(noor,1:NNpsf) = i_masq(ind_p(noor),1:NNpsf);

  vm = vois.*masq;
  pm = psf.*masq;
  ddd = sum(pm.*(pm-2*vm),2);
  weights_n = exp(-0.5/(intensity*intensity)*ddd)';

  % ---- normalization
  
  sum_weights_n = sum(weights_n);
  
  if sum_weights_n == 0
    disp(['         warning : null sum at frame #',num2str(n)])
    weights_n = ones(1,nbp)/nbp;
  else
    weights_n = weights_n / sum_weights_n;
  end
  
  weights(n,:) = weights_n;

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % step 2 : selection
  % ------------------------------------------------------------------------
  %     compute the corrected particles p..(n,2,.)
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  weights_n  = weights_n / sum(weights_n);
  offsprings = f_resample_residual(weights_n);
  indices    = f_resample_indices(offsprings);

  weights(n,:) = ones(1,nbp)/nbp;

  prx(n,2,:) = prx(n,1,indices);
  pry(n,2,:) = pry(n,1,indices);
  alpha_x(1:2,1:nbp) = alpha_x(1:2,indices(1:nbp));
  alpha_y(1:2,1:nbp) = alpha_y(1:2,indices(1:nbp));
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % step 3 : prediction
  % ------------------------------------------------------------------------
  %     compute the predicted particles p..(n+1,2,.)
  %     [WARNING: we predict the particle at time nb_frames+1...]
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  r_x = squeeze(prx(n,2,:))';
  r_y = squeeze(pry(n,2,:))';
  
  alpha_x_m = Faa * alpha_x ;
  P_x_m     = Faa * P_x * Faa' + Qaa;

  r_hat_x_m = Fba * alpha_x + r_x;
  Xi_x      = Fba * P_x * Fba' + Qbb;
  r_x_m = r_hat_x_m + sqrt(Xi_x)*randn(1,nbp);
  S_x      = Faa * P_x * Fba' + Qab;
  alpha_x  = alpha_x_m + (1/Xi_x) *...
      repmat(r_x_m - r_hat_x_m,2,1) .* repmat(S_x,1,nbp);
  P_x      = P_x_m - (1/Xi_x) * S_x * S_x';

  alpha_y_m = Faa * alpha_y ;
  P_y_m     = Faa * P_y * Faa' + Qaa;
  r_hat_y_m = Fba * alpha_y + r_y;
  Xi_y      = Fba * P_y * Fba' + Qbb;
  r_y_m = r_hat_y_m + sqrt(Xi_y)*randn(1,nbp);
  S_y      = Faa * P_y * Fba' + Qab;
  alpha_y  = alpha_y_m + (1/Xi_y) *...
      repmat(r_y_m - r_hat_y_m,2,1) .* repmat(S_y,1,nbp);
  P_y      = P_y_m - (1/Xi_y) * S_y * S_y';
  

  prx(n+1,1,:) = r_x_m(:)' ;
  pry(n+1,1,:) = r_y_m(:)';

  pax(n,2,:) = alpha_x(1,:);
  pay(n,2,:) = alpha_y(1,:);
  pvx(n,2,:) = alpha_x(2,:);
  pvy(n,2,:) = alpha_y(2,:);

  pax(n+1,1,:) = alpha_x_m(1,:);
  pay(n+1,1,:) = alpha_y_m(1,:);
  pvx(n+1,1,:) = alpha_x_m(2,:);
  pvy(n+1,1,:) = alpha_y_m(2,:);
  
end

disp(['     total CPU ' num2str(toc) ' s.']);
close(h_bar2) 

% --- computing statistics -------------------------------------------------

% --- for r
ppx             = squeeze(prx(:,1,:));
ppy             = squeeze(pry(:,1,:));
[m_rx1,m_ry1]   = f_comp_mean(weights,ppx,ppy);

ppx             = squeeze(prx(:,2,:));
ppy             = squeeze(pry(:,2,:));
[m_rx,m_ry]     = f_comp_mean(weights,ppx,ppy);
[c_rxx,c_rxy,c_ryy] = f_comp_cov(weights,ppx,ppy);
% msre = f_comp_msre(weights,ppx,ppy);
[map_rx,map_ry] = f_comp_map(weights,ppx,ppy);

% --- for v
ppx             = squeeze(pvx(:,1,:));
ppy             = squeeze(pvy(:,1,:));
[m_vx1,m_vy1]   = f_comp_mean(weights,ppx,ppy);

ppx             = squeeze(pvx(:,2,:));
ppy             = squeeze(pvy(:,2,:));
[m_vx,m_vy]     = f_comp_mean(weights,ppx,ppy);

% --- for a
ppx             = squeeze(pax(:,1,:));
ppy             = squeeze(pay(:,1,:));
[m_ax1,m_ay1]   = f_comp_mean(weights,ppx,ppy);

ppx             = squeeze(pax(:,2,:));
ppy             = squeeze(pay(:,2,:));
[m_ax,m_ay]     = f_comp_mean(weights,ppx,ppy);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART 5 - plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

psize = 1;

% --- parameters -------------------------------------------------------------

observed_sequence_max=max(max(max(observed_sequence)));
observed_sequence_min=min(min(min(observed_sequence)));
observed_sequence_range=[observed_sequence_min observed_sequence_max];

vx_min = min(min(min(squeeze(pvx(:,1,:)))),t_vx_min);
vx_max = max(max(max(squeeze(pvx(:,1,:)))),t_vx_max);
vy_min = min(min(min(squeeze(pvy(:,1,:)))),t_vy_min);
vy_max = max(max(max(squeeze(pvy(:,1,:)))),t_vy_max);

ax_min = min(min(min(squeeze(pax(:,1,:)))),t_ax_min);
ax_max = max(max(max(squeeze(pax(:,1,:)))),t_ax_max);
ay_min = min(min(min(squeeze(pay(:,1,:)))),t_ay_min);
ay_max = max(max(max(squeeze(pay(:,1,:)))),t_ay_max);

% ----------------------------------------------------------------------------

while 1 %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
  disp('   ');
  disp(' >>> plotting ');

  fprintf(['    \n' ...
	   ' >>> a new plot ? \n' ...
	   '         1. errors \n' ...
	   '         2. particules evolution (prediction/correction) \n' ...
	   '         3. particules evolution (+ empirical mean & cov) \n' ...
	   '         4. observations (+ thresholds) \n' ...
	   '         5. observations (+ estimators & map) \n' ...
	   '         q. exit \n']);
  reponse=input('     choice [1] : ','s');
  if isempty(reponse),  reponse = '1'; , end

  switch reponse
    
    %%%% error and criteria %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   case '1'
    
    set(gcf,'DoubleBuffer','on');
    clf
    
    error_dist_mean = sqrt( (m_rx-t_rx).^2 + (m_ry-t_ry).^2);
    
    plot([1:nb_frames],error_dist_mean,'r')
    hold on
    %     plot([1:nb_frames],sqrt(msre),'g')
    plot([1 nb_frames],[delta_ry delta_ry],'b--')
    legend('error','pixel size');
    %    legend('mean','sqrt msre','pixel size');
    title('estimation errors')
    % axis([1 nb_frames 0 1.2*max(max(error_dist_mean),max(error_dist_map))])
    axis([1 nb_frames 0 4*delta_ry])
    
    %%%% particles' evolution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %    particles for position, speed, acceleration
    %    both prediction/correction
   case '2'

    clf

    set(gcf,'DoubleBuffer','on');
    
    for n=1:nb_frames
      
      % --- prediction ---------------------------------------------------------

      clf
      
      % ----------------
      subplot(221)
      axis xy ; axis square ; axis([rx_min rx_max ry_min ry_max ])
      hold on
      colormap(gray)
      xlabel('position')
      title(['frame #',num2str(n)])
      plot(squeeze(prx(n,1,:)),squeeze(pry(n,1,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','g');
      plot(t_rx(1:n),t_ry(1:n),'r-');
      plot(t_rx(n),t_ry(n),'r+');
      % ----------------
      subplot(222)
      axis xy ; axis equal ; axis([vx_min vx_max vy_min vy_max ])
      hold on
      colormap(gray)
      xlabel('speed')
      plot(squeeze(pvx(n,1,:)),squeeze(pvy(n,1,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','g');
      plot(t_vx(1:min(n,nb_frames-1)),t_vy(1:min(n,nb_frames-1)),'r-');
      plot(t_vx(min(n,nb_frames-1)),t_vy(min(n,nb_frames-1)),'r+');
      % ----------------
      subplot(223)
      axis xy ; axis equal ; axis([ax_min ax_max ay_min ay_max ])
      hold on
      colormap(gray)
      xlabel('acceleration')
      plot(squeeze(pax(n,1,:)),squeeze(pay(n,1,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','g');
      plot(t_ax(min(n,nb_frames-2)),t_ay(min(n,nb_frames-2)),'r+');
      plot(t_ax(1:min(n,nb_frames-2)),t_ay(1:min(n,nb_frames-2)),'r-');
      % ----------------
      
      drawnow

      % --- correction ---------------------------------------------------------

      clf
      
      % ----------------
      subplot(221)
      axis xy ; axis square ; axis ([rx_min rx_max ry_min ry_max ])
      hold on
      colormap(gray)
      xlabel('position')
      title(['frame #',num2str(n)])
      plot(squeeze(prx(n,2,:)),squeeze(pry(n,2,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','b');
      plot(t_rx(1:n),t_ry(1:n),'r-');
      plot(t_rx(n),t_ry(n),'r+');
      % ----------------
      subplot(222)
      axis xy ; axis equal ; axis([vx_min vx_max vy_min vy_max ])
      hold on
      colormap(gray)
      xlabel('speed')

      plot(squeeze(pvx(n,2,:)),squeeze(pvy(n,2,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','b');
      plot(t_vx(min(n,nb_frames-1)),t_vy(min(n,nb_frames-1)),'r+');
      plot(t_vx(1:min(n,nb_frames-1)),t_vy(1:min(n,nb_frames-1)),'r-');
      % ----------------
      subplot(223)
      axis xy ; axis equal ; axis ([ax_min ax_max ay_min ay_max ])
      hold on
      colormap(gray)
      xlabel('acceleration')
      plot(squeeze(pax(n,2,:)),squeeze(pay(n,2,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','b');
      plot(t_ax(1:min(n,nb_frames-2)),t_ay(1:min(n,nb_frames-2)),'r-');
      plot(t_ax(min(n,nb_frames-2)),t_ay(min(n,nb_frames-2)),'r+');
      % ----------------
      
      drawnow
      
    end

    % summing up

    f_pause_plot
    
    clf
      
    % ----------------
    subplot(221)
    axis xy ; axis square ; axis([rx_min rx_max ry_min ry_max ])
    hold on
    colormap(gray)
    xlabel('position')
    % plot(t_rx(1:n),t_ry(1:n),...
	% 'Marker','o','MarkerSize',4,'Color','r');
    % plot(m_rx(1:n),m_ry(1:n),...
	% 'Marker','o','MarkerSize',4,'Color','g');
    plot(t_rx(1:n),t_ry(1:n),'r');
    plot(m_rx(1:n),m_ry(1:n),'g');
    plot([ini_zone(1) ini_zone(2) ini_zone(2) ini_zone(1) ini_zone(1)],...
	 [ini_zone(3) ini_zone(3) ini_zone(4) ini_zone(4) ini_zone(3)],'b');
    plot(m_rx1(1),m_ry1(1),'LineStyle','none','Marker','o','MarkerSize',4,'Color','b');
    % ----------------
    subplot(222)
    axis xy ; axis equal ; axis([vx_min vx_max vy_min vy_max ])
    hold on
    colormap(gray)
    xlabel('speed')
    plot(t_vx(1:nb_frames-1),t_vy(1:nb_frames-1),...
		 'Marker','o','MarkerSize',4,'Color','r');
     plot(m_vx(1:nb_frames-1),m_vy(1:nb_frames-1),...
	 'Marker','o','MarkerSize',4,'Color','g');
    plot([ini_zone(1+4) ini_zone(2+4) ini_zone(2+4) ini_zone(1+4) ini_zone(1+4)],...
	 [ini_zone(3+4) ini_zone(3+4) ini_zone(4+4) ini_zone(4+4) ini_zone(3+4)],'b');
    plot(m_vx1(1),m_vy1(1),'LineStyle','none','Marker','o','MarkerSize',4,'Color','b');
    % ----------------
    subplot(223)
    axis xy ; axis equal ; axis([ax_min ax_max ay_min ay_max ])
    hold on
    colormap(gray)
    xlabel('acceleration')
    plot(t_ax(1:nb_frames-2),t_ay(1:nb_frames-2),...
	 'Marker','o','MarkerSize',4,'Color','r');
    plot(m_ax(1:nb_frames-2),m_ay(1:nb_frames-2),...
	 'Marker','o','MarkerSize',4,'Color','g');
    plot([ini_zone(1+8) ini_zone(2+8) ini_zone(2+8) ini_zone(1+8) ini_zone(1+8)],...
	 [ini_zone(3+8) ini_zone(3+8) ini_zone(4+8) ini_zone(4+8) ini_zone(3+8)],'b');
    plot(m_ax1(1),m_ay1(1),'LineStyle','none','Marker','o','MarkerSize',4,'Color','b');
    % ----------------
    
    drawnow

      
    %%%% evolution of the particules %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %    just position particles
    %    only correction step
   case '3'

    set(gcf,'DoubleBuffer','on');
    
    for n=1:nb_frames
      
      % --- correction ---------------------------------------------------------

      clf
      
      % ----------------
      axis xy
      axis square
      hold on
      colormap(gray)
      axis ([rx_min rx_max ry_min ry_max ])
      xlabel('position (with cond. mean & cov)')
      title(['frame #',num2str(n)])
      plot(t_rx(1:n),t_ry(1:n),...
	   'LineStyle','none','Marker','o','MarkerSize',4,'Color','r');
      plot(m_rx(1:n),m_ry(1:n),...
	   'LineStyle','none','Marker','o','MarkerSize',4,'Color','g');
      plot(squeeze(prx(n,2,:)),squeeze(pry(n,2,:)),...
	   'LineStyle','none','MarkerSize',psize,...
	   'Marker','.','Color','b');
      f_plot_ellipse([m_rx(n) ; m_ry(n)],...
		     [c_rxx(n) c_rxy(n) ; c_rxy(n) c_ryy(n)],20,'g')
      % ----------------
      
      drawnow
%      pause (0.5)
      
    end
    
    % summing up
    f_pause_continue
    clf
    axis xy
    axis square
    hold on
    colormap(gray)
    axis ([rx_min rx_max ry_min ry_max ])
    xlabel('position (true and estimated)')
    plot(t_rx(1:n),t_ry(1:n),...
	 'Marker','o','MarkerSize',4,'Color','r');
    plot(m_rx(1:n),m_ry(1:n),...
	 'LineStyle','none','Marker','o','MarkerSize',4,'Color','g');
    plot([ini_zone(1) ini_zone(2) ini_zone(2) ini_zone(1) ini_zone(1)],...
	 [ini_zone(3) ini_zone(3) ini_zone(4) ini_zone(4) ini_zone(3)],'b');
    plot(m_rx1(1),m_ry1(1),'LineStyle','none','Marker','o','MarkerSize',4,'Color','b');
    %    legend('true position','estimate','initial guess area');
    drawnow

    
    %%%% observations + thresholding %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   case '4'

    for n=1:nb_frames
  
      i_observed(:,:) = observed_sequence(n,:,:);
      
      clf
      
      % ---------------------------------------
      subplot(221)
      imagesc(coord_x,coord_y,i_observed');
      set(gca,'CLim',observed_sequence_range);
      hold on
      colormap(gray)
      axis ([rx_min rx_max ry_min ry_max ])
      axis square
      axis xy
      title(['observation, frame #',num2str(n)])
      % ---------------------------------------
      subplot(222)
      imagesc(coord_x,coord_y,i_observed');
      set(gca,'CLim',observed_sequence_range);
      hold on
      colormap(gray)
      plot(t_rx(n),t_ry(n),'r+');
      axis ([rx_min rx_max ry_min ry_max ])
      axis square
      axis xy
      title('with the real position')
      % ---------------------------------------
      subplot(224)
      imagesc(coord_x,coord_y,(i_observed>=2*intensity)');
      hold on
      colormap(gray)
      plot(t_rx(n),t_ry(n),'r+');
      axis ([rx_min rx_max ry_min ry_max ])
      axis square
      axis xy
      title('pixels over 2 * standard-deviation of noise')
      % ---------------------------------------
      
      drawnow
      
      % f_pause_plot
      
    end
    
    %%%% observations + estimators + map %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   case '5'
    
    [pix_x,pix_y]=f_r2p(prx(1,2,:),pry(1,2,:),rx_min,ry_min,delta_rx,delta_ry);
    
    clf
    
    for n=1:nb_frames
      
      [pix_x,pix_y]=f_r2p(prx(n,2,:),pry(n,2,:),rx_min,ry_min,delta_rx,delta_ry);
      % pic of the map
      i_map = zeros(n_px,n_py);
      for p=1:nbp
        px = pix_x(p);
        py = pix_y(p);
        if ((1 <= px) & ( px <=  n_px)) & ((1 <= py) & ( py <=  n_py))
          i_map(px,py)=i_map(px,py)+weights(n,p);
        end
      end    
      
      [maxou,maxi,maxj]=f_max_mat(i_map);
      
      % clf
      subplot(221)
      plot(t_rx(1:n),t_ry(1:n),'r-');
      hold on
      size_of_marker=22*10/min(n_px,n_py);
      [mmaxi,mmaxj]=f_p2r(maxj(:),maxi(:),rx_min,ry_min,delta_rx,delta_ry);
      % here (previous line) a huge trick: as we plot the
      % transposed pictures we should inverse  maxi(:) and maxj(:)
      plot(mmaxi,mmaxj,...
           'LineStyle','none','MarkerSize',size_of_marker,...
           'Marker','s','Color','g');
      plot(m_rx(1:n),m_ry(1:n),'b-');
      axis ([rx_min rx_max ry_min ry_max ])
      axis square
      axis xy
      title(['frame #',num2str(n)])
      xlabel('mean + pixel map')
      
      subplot(222)
      imagesc(coord_x,coord_y,i_map');
      colormap summer
      hold on
      plot(t_rx(1:n),t_ry(1:n),'r-');
      plot(t_rx(n),t_ry(n),'r+');
      axis ([rx_min rx_max ry_min ry_max ])
      axis square
      axis xy
      xlabel('pixelized conditionnal density')
      drawnow
      
      drawnow
      
    end
    
    %%%% exit %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   case 'q'
    
    break
    
  end
  
end %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

