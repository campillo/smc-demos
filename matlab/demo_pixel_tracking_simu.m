%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function demo_pixel_tracking_simu()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tracking a mobile in a sequence of pics
% - RB
% - SPF (the mobile modify more than one pixel)
% - state model: with constant acceleration
%      \dot r = v, \dot v = a, \dot a = bbg (independently in x and y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART 1 - input parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

demo_pixel_tracking_parameters_simu
demo_pixel_tracking_parameters_filter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART 2 - simulation of the trajectory of the mobile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

validate_trajectory = 1;

while validate_trajectory %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			  
  disp('   ');
  disp(' >>> mobile trajectory')

  % initial conditions of the simulated trajectory
  t_rx(1) = 0;
  t_ry(1) = 0;
  %  t_rx(1) = rx_min + (rx_max-rx_min)*rand;
  %  t_ry(1) = ry_min + (ry_max-ry_min)*rand;
  t_vx(1) = vx_min + (vx_max-vx_min)*rand;
  t_vy(1) = vy_min + (vy_max-vy_min)*rand;
  t_ax(1) = ax_min + (ax_max-ax_min)*rand;
  t_ay(1) = ay_min + (ay_max-ay_min)*rand;
  
  for n=2:nb_frames

    w1 = randn;
    w2 = randn;
    w3 = randn;
    t_ax(n) = t_ax(n-1) ...
	      + scovw(1,1)*w1 + scovw(1,2)*w2 + scovw(1,3)*w3 ;
    t_vx(n) = t_vx(n-1) + delta_t*t_ax(n-1) ...
	      + scovw(2,1)*w1 + scovw(2,2)*w2 + scovw(2,3)*w3 ;
    t_rx(n) = t_rx(n-1) + delta_t*t_vx(n-1) + 0.5*delta_t*delta_t*t_ax(n-1) ...
	      + scovw(3,1)*w1 + scovw(3,2)*w2 + scovw(3,3)*w3 ;

    w1 = randn;
    w2 = randn;
    w3 = randn;
    t_ay(n) = t_ay(n-1) ...
	      + scovw(1,1)*w1 + scovw(1,2)*w2 + scovw(1,3)*w3 ;
    t_vy(n) = t_vy(n-1) + delta_t*t_ay(n-1) ...
	      + scovw(2,1)*w1 + scovw(2,2)*w2 + scovw(2,3)*w3 ;
    t_ry(n) = t_ry(n-1) + delta_t*t_vy(n-1) + 0.5*delta_t*delta_t*t_ay(n-1) ...
	      + scovw(3,1)*w1 + scovw(3,2)*w2 + scovw(3,3)*w3 ;

  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % we compute the speed and acceleration then we sum up on 3
  % windows : position - speed - acceleration 
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  clf

  % --- position
  subplot(221)
  hold on
  axis([rx_min rx_max ry_min ry_max]);
  axis xy                       % standard spatial origin place
  axis square                   % squared image 
  title('position')
  plot(t_rx,t_ry,'r+');  

  % --- speed
  subplot(222)
  plot(t_vx,t_vy,'r+');  
  axis xy ; 
  axis equal ; 
  title('speed')
  
  % --- acceleration
  subplot(223)
  plot(t_ax,t_ay,'r+');  
  axis xy                      % standard spatial origin place
  axis equal                   % squared image 
  title('acceleration')
  
  drawnow

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % --- some statistics about the mobile's dynamic
  
  t_rx_min = min(t_rx);
  t_rx_max = max(t_rx);
  t_ry_min = min(t_ry);
  t_ry_max = max(t_ry);
  
  t_vx_min = min(t_vx(1:end-1));
  t_vx_max = max(t_vx(1:end-1));
  t_vy_min = min(t_vy(1:end-1));
  t_vy_max = max(t_vy(1:end-1));
  
  t_ax_min = min(t_ax(1:end-2));
  t_ax_max = max(t_ax(1:end-2));
  t_ay_min = min(t_ay(1:end-2));
  t_ay_max = max(t_ay(1:end-2));
  
  % -------------------------------------------------
  answer_traj = input('       accept this trajectory ? y/n [y] : ','s');
  if isempty(answer_traj),  answer_traj = 'y'; , end
  if answer_traj == 'n'
    validate_trajectory = 1;
  else
    validate_trajectory = 0;
  end
  % -------------------------------------------------
  

end %<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART 3 - simulation of the sequence of images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('   ');
disp(' >>> simulation of the sequence of images ');

% --- we simulate the observed pics ---------------------------------------

h_bar = waitbar(0,'Simulation, please wait...');


for n=1:nb_frames

  waitbar(n/nb_frames)

  rx = t_rx(n);
  ry = t_ry(n);
  [px,py] = f_r2p(rx,ry,rx_min,ry_min,delta_rx,delta_ry);

  % --- the psf
  psf = f_psf(rx,ry,rx_min,ry_min,delta_rx,delta_ry,Npsf,sigma2psf);
  
  % --- pic of the mobile				
  i_mobile = zeros(n_px,n_py);
  ix = [max(px-Npsf,1):min(px+Npsf,n_px)]; % indices of the touched
  iy = [max(py-Npsf,1):min(py+Npsf,n_py)]; % pixels
  i_mobile(ix,iy) = psf(ix-px+Npsf+1,iy-py+Npsf+1);
  
  % --- noise pic
  i_bruit = intensity*randn(n_px,n_py);

  % --- observed pic
  %  i_observed = i_mobile;
  i_observed = i_bruit + i_mobile;
  observed_sequence(n,:,:) = i_observed(:,:);

end

close(h_bar) 

sequence_range_min = min(min(min(observed_sequence)));
sequence_range_max = max(max(max(observed_sequence)));

range_normal = [round(sequence_range_min) round(sequence_range_max)];

% --- initialisation RB ----------------------------------------------------

P_x = 1 * [ 1 0 ; 0 1];
P_y = 1 * [ 1 0 ; 0 1];


save('demo_pixel_tracking_workspace.mat')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART 5 - plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

psize = 1;

% --- parameters -------------------------------------------------------------

observed_sequence_max=max(max(max(observed_sequence)));
observed_sequence_min=min(min(min(observed_sequence)));
observed_sequence_range=[observed_sequence_min observed_sequence_max];



% ----------------------------------------------------------------------------


for n=1:nb_frames
  
  i_observed(:,:) = observed_sequence(n,:,:);

  clf

  % ---------------------------------------
  subplot(221)
  imagesc(coord_x,coord_y,i_observed');
  set(gca,'CLim',observed_sequence_range);
  hold on
  colormap(gray)
  axis ([rx_min rx_max ry_min ry_max ])
  axis square
  axis xy
  title(['observation, frame #',num2str(n)])
  % ---------------------------------------
  subplot(222)
  imagesc(coord_x,coord_y,i_observed');
  set(gca,'CLim',observed_sequence_range);
  hold on
  colormap(gray)
  plot(t_rx(n),t_ry(n),'r+');
  axis ([rx_min rx_max ry_min ry_max ])
  axis square
  axis xy
  title('with the real position')
  % ---------------------------------------
  subplot(224)
  imagesc(coord_x,coord_y,(i_observed>=2*intensity)');
  hold on
  colormap(gray)
  plot(t_rx(n),t_ry(n),'r+');
  axis ([rx_min rx_max ry_min ry_max ])
  axis square
  axis xy
  title('pixels over 2 * standard-deviation of the noise')
  % ---------------------------------------
  
  drawnow
  
  pause(0.3)
  
end

