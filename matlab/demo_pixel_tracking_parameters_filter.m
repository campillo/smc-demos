%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


% >>> FILTER PARAMETERS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

sigma2_q = 120 ;  %> variance of the state noise
nbp      = 2000;  %> number of particles

ini_rx_ecart = 40;
ini_vx_ecart = 20;
ini_ax_ecart = 20;

Npsf = 1 ;                     %> parameter for the point spread function 
sigma2psf = 50 ;               %> "

% --- derived parameters

NNpsf = (2*Npsf+1)*(2*Npsf+1);

% covariace matrice for the state noise
covw = sigma2_q * [delta_t        delta_t^2/2     delta_t^3/6 ;
		   delta_t^2/2    delta_t^3/3     delta_t^4/8 ;
		   delta_t^3/6    delta_t^4/8     delta_t^5/20 ];
scovw = sqrtm(covw);

% the RB matrices
Faa = [1       0; 
       delta_t 1];
Fba = [delta_t*delta_t/2 delta_t];
Qaa = sigma2_q * [delta_t            delta_t*delta_t/2  ; 
		  delta_t*delta_t/2  delta_t*delta_t*delta_t/3 ];
Qba = sigma2_q * [delta_t            delta_t*delta_t/2  ; 
		  delta_t*delta_t/2  delta_t*delta_t*delta_t/3 ];
Qab = sigma2_q * [delta_t^3/6   ;
		  delta_t^4/8];
Qbb = sigma2_q * delta_t^5/20;
