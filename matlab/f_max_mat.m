%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================



function [le_maximum,arg_i,arg_j]=f_max_mat(matrice)
%------------------------------------------------------------------------------
% the maximum of the 2D matrix with the arg of the maximum
%------------------------------------------------------------------------------
[f_max_mat_j,max_j]  = max(matrice);
[le_maximum,max_i] = max(f_max_mat_j);
arg_i              = max_i;
arg_j              = max_j(max_i);


