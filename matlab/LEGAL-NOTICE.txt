LEGAL NOTICE

SMC DEMOS, version 1.0, Copyright © Fabien Campillo - INRIA 2009

Software: SMC DEMOS, version 1.0 of March 2009, hereinafter referred
to as "the software".

The Software has been designed and implemented by Fabien Campillo /
INRIA  hereinafter referred to as "the owner", holds all the
ownership rights on the Software. The Software has been registered at
the Agence pour la Protection des Programmes (APP), under reference

	IDDN.FR.001.28003.000.S.P.2009.000.31235


This software is being provided by the owner under the following
license. By obtaining, using and/or copying this software, you agree
that you have read, understood, and will comply with the following
terms and conditions:

Permission to use, copy, modify, and distribute the software and its
documentation for any purpose and without fee or royalty is hereby
granted, provided that the full text of this NOTICE (legal-notice.txt)
appears on ALL copies of the software and documentation or portions
thereof, including modifications, that you make. 

The Owner freely grants the right to use the Software for
non-commercial purposes. Any use or reproduction of the Software to
obtain profit or for commercial ends being subject to obtaining the
prior express authorization of the Owner.


THE SOFTWARE IS PROVIDED "AS IS," AND THE OWNER MAKES NO 
REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF EXAMPLE,
BUT NOT LIMITATION, THE OWNER MAKES NO REPRESENTATIONS OR WARRANTIES
OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE
USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY
PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. THE OWNER WILL BEAR
NO LIABILITY FOR ANY USE OF THIS SOFTWARE OR DOCUMENTATION.

The name and trademarks of the owner may NOT be used in advertising or
publicity pertaining to the software without specific, written prior
permission. Title to copyright in this software and any associated
documentation will at all times remain with copyright holder.

