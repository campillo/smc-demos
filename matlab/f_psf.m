%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function psf=f_psf(rx,ry,rx_min,ry_min,delta_rx,delta_ry,Npsf,sigma2psf)
%------------------------------------------------------------------------------
% associated pixel
[px,py]   = f_r2p(rx,ry,rx_min,ry_min,delta_rx,delta_ry);
% coordinates of the center of the pixel
[rrx,rry] = f_p2r(px,py,rx_min,ry_min,delta_rx,delta_ry);
% building the PSF matrix
[xx,yy] = meshgrid([-Npsf:+Npsf],[-Npsf:+Npsf]);
% warning:
% the prime on xx and yy fixes the same problem
% of x <-> y but why ?...
% xx      = delta_rx * xx - (rx-rrx);
% yy      = delta_ry * yy - (ry-rry);
xx      = delta_rx * xx' - (rx-rrx);
yy      = delta_ry * yy' - (ry-rry);
psf     = -0.5/sigma2psf * (xx.*xx + yy.*yy);
psf     = psf-psf(Npsf+1,Npsf+1);
%psf     = psf-max(max(psf));
psf     = exp(psf);

%------------------------------------------------------------------------------
%function psf=f_psf_save(rx,ry,rx_min,ry_min,delta_rx,delta_ry,Npsf,sigma2psf)
% pixel associ�
%[px,py]   = f_r2p(rx,ry,rx_min,ry_min,delta_rx,delta_ry);
% coordonn�es du centre du pixel
%[rrx,rry] = f_p2r(px,py,rx_min,ry_min,delta_rx,delta_ry);
% construction de la matrice PSF
%[xx,yy] = meshgrid([-Npsf:+Npsf],[-Npsf:+Npsf]);
%xx      = delta_rx * xx - (rx-rrx);
%yy      = delta_ry * yy - (ry-rry);
%psf     = -0.5/sigma2psf * (xx.*xx + yy.*yy);
%psf     = psf-max(max(psf));
%psf     = exp(psf);


