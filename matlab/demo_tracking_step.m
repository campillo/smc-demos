%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function demo_tracking_step()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (C) Fabien Campillo - INRIA
%--------------------------------------------------------------------------
% The idea of this demonstration is due to Simon Maskell (presented
% at the IHP)
%--------------------------------------------------------------------------
% created on 8 June 2003 - modified on January 16, 2009
% - Sep  6, 2003 : the inner loop (particle loop) is vectorized (!)
% - Sep  6, 2003 : the inner loop (particle loop) is vectorized (!)
% - Jan 16, 2009 : cleaning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('  ');
disp('  ');
disp('---- demo: bearing only tracking with obstacles      ');
disp('           idea due to Simon Maskell                 ');
disp('           (step by step version)                    ');
disp('  ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% constant parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

X1_MIN = -10000 ; % the state space domain
X1_MAX =  10000 ; %          "
X2_MIN = -10000 ; %          "
X2_MAX =  10000 ; %          "

SIGMA_OBS = 1 * pi/180.; % standard deviation for the observation noise
SIGMA2_OBS = SIGMA_OBS*SIGMA_OBS;

N_PART  = 5000 ; % number of particles
SIGMA_W = 400;   % standard deviation for the state process noise

NUMBER_OF_OBS = 200; % number of observations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' >>> STEP 1: interactive contruction of the geometry of the problem')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- the observation sites --------------------------------------------------

disp('     - place the observation sites:')
disp('           [left button to choose a point')
disp('           [right button for the last point')

clf;
axis([X1_MIN X1_MAX X2_MIN X2_MAX ]);
axis square;
hold on;


x = [];
y = [];
n = 0;

goal = 1;
while goal == 1
    [xi,yi,goal] = ginput(1);
    plot(xi,yi,...
        'gs','LineStyle','none','MarkerEdgeColor','k',...
        'MarkerFaceColor','g','MarkerSize',10)
    n = n+1;
    x(n,1) = xi;
    y(n,1) = yi;
end

X1_SITES = x;
X2_SITES = y;
N_SITES  = n;

set(gcf,'position',[100 100 600 500])


% --- observation obstacle segments ---------------------------------------

disp('   ');
disp('     - place observation obstacle segments:')
disp('           [left button to choose a point')
disp('           [right button to choose the last point')
disp('           [warning: choose an even number of points')

x1a = [];
x2a = [];
x1b = [];
x2b = [];
n = 0;

goal = 1;
while goal == 1
    [x1i,x2i,goal] = ginput(2);
    plot(x1i,x2i,'black','LineWidth',2);
    drawnow;
    n = n+1;
    x1a(n,1) = x1i(1);  % point a co-ordinate #1
    x2a(n,1) = x2i(1);  % point a co-ordinate #2
    x1b(n,1) = x1i(2);  % point b co-ordinate #1
    x2b(n,1) = x2i(2);  % point b co-ordinate #2
end

X1A_SEGMENTS = x1a ;
X2A_SEGMENTS = x2a ;
X1B_SEGMENTS = x1b ;
X2B_SEGMENTS = x2b ;
N_SEGMENTS= n;

% the trajectory ----------------------------------------------------------

disp('   ');
disp('     - trajectory of the mobile:')
disp('           [left button to choose a point')
disp('           [right button for the last point')

x = [];
y = [];
n = 0;

goal = 1;
while goal == 1
    [xi,yi,goal] = ginput(1);
    plot(xi,yi,'r+')
    n = n+1;
    x(n,1) = xi;
    y(n,1) = yi;
end

% Interpolate with two splines and finer spacing.
t       = 1:n;
delta   = (n-1)/NUMBER_OF_OBS; % 200 is the number of points of the
% trajectory, i.e. the number of observations
ts      = 1: delta: n;
X1_TRAJ = spline(t,x,ts);
X2_TRAJ = spline(t,y,ts);
N_TRAJ  = length(ts) ;

plot(X1_TRAJ,X2_TRAJ,'r-');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' >>> STEP 2: observation simulation')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('           [press RETURN to proceed] ... ');
pause
fprintf(' \n');

observation = zeros(2,N_SITES,N_TRAJ);

for n=1:N_TRAJ
    clf;
    axis([X1_MIN X1_MAX X2_MIN X2_MAX ]);
    axis square;
    hold on;
    for n_seg=1:N_SEGMENTS
        plot([X1A_SEGMENTS(n_seg) X1B_SEGMENTS(n_seg)],...
            [X2A_SEGMENTS(n_seg) X2B_SEGMENTS(n_seg)],...
            'black','LineWidth',2);
    end
    plot(X1_SITES,X2_SITES,...
        'gs','LineStyle','none','MarkerEdgeColor','k',...
        'MarkerFaceColor','w','MarkerSize',10)
    plot(X1_TRAJ,X2_TRAJ,'r-');
    plot(X1_TRAJ(n),X2_TRAJ(n),'r+');
    for n_site=1:N_SITES
        % simulation of the observation of the site n_site
        % it is checked if the site n_site sees
        % the mobile X1_TRAJ(n), X2_TRAJ(n)
        % if only one segment hides the mobile:  nothing is seen
        hidden = 0;
        for n_seg=1:N_SEGMENTS
            % one check if the site n_site is hidden by the segment n_seg
            mask = intersect(X1_SITES(n_site),  X2_SITES(n_site),...
                X1_TRAJ(n),          X2_TRAJ(n),...
                X1A_SEGMENTS(n_seg), X2A_SEGMENTS(n_seg),...
                X1B_SEGMENTS(n_seg), X2B_SEGMENTS(n_seg));

            if mask==1
                hidden = 1; % one occurence is enough for being hidden
            end
        end

        if hidden==0
            % the mobile is not hidden from the site
            observation(1,n_site,n) = 1;
            observation(2,n_site,n) = ...
                atan2(X1_TRAJ(n)-X1_SITES(n_site),...
                X2_TRAJ(n)-X2_SITES(n_site))  + SIGMA_OBS * randn;
            plot([X1_SITES(n_site) X1_TRAJ(n)],...
                [X2_SITES(n_site) X2_TRAJ(n)],'--g');
            plot([X1_SITES(n_site) ...
                X1_SITES(n_site)+2000*cos(pi/2.-observation(2,n_site,n))],...
                [X2_SITES(n_site) ...
                X2_SITES(n_site)+2000*sin(pi/2.-observation(2,n_site,n))],'r');
            plot(X1_SITES(n_site),X2_SITES(n_site),...
                'gs','LineStyle','none','MarkerEdgeColor','k',...
                'MarkerFaceColor','g','MarkerSize',10)
        end

    end

    drawnow;

    set(gcf,'DoubleBuffer','on')

end

pause

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(' >>> STEP 3:  filter');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('           [press RETURN to proceed] ... ');
pause
fprintf(' \n');

% --- particles initialization --------------------------------------------

X1 = X1_MIN + (X1_MAX-X1_MIN) * randn(1,N_PART);
X2 = X2_MIN + (X2_MAX-X2_MIN) * randn(1,N_PART);

% --- iterations ----------------------------------------------------------

for n=1:N_TRAJ % begining of the time loop

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % prediction
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    X1p = X1 + SIGMA_W * randn(1,N_PART);
    X2p = X2 + SIGMA_W * randn(1,N_PART);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % likelihood
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    lklhd  = ones(1,N_PART);

    for n_site=1:N_SITES

        % vectorized in particles
        vv(1:N_PART) = visibles(X1_SITES(n_site),X2_SITES(n_site), ...
            X1p(1:N_PART),X2p(1:N_PART), ...
            N_SEGMENTS,X1A_SEGMENTS,X2A_SEGMENTS,...
            X1B_SEGMENTS,X2B_SEGMENTS);

        lklhd_inst = zeros(1,N_PART);

        if observation(1,n_site,n) == 1

            % --- case 1 - the site sees the mobile
            logl(1:N_PART) =  observation(2,n_site,n) ...
                - observation(1,n_site,n) ...
                * atan2(X1p(1:N_PART)-X1_SITES(n_site),...
                X2p(1:N_PART)-X2_SITES(n_site));
            lklhd_inst1(1:N_PART) = ...
                exp(-1/SIGMA2_OBS*logl(1:N_PART).*logl(1:N_PART));
            lklhd_inst(vv~=0) = lklhd_inst1(vv~=0);

        else

            % --- case 2 - the site does not see the mobile
            lklhd_inst(vv==0) = 1;

        end

        lklhd=lklhd.*lklhd_inst;

    end

    % normalization
    normalization = sum(lklhd);
    if normalization == 0
        lklhd = ones(1,N_PART)/N_PART;
        disp('  warning : null likelihood -> particles positions reset');
        X1p = X1_MIN + (X1_MAX-X1_MIN) * randn(1,N_PART);
        X2p = X2_MIN + (X2_MAX-X2_MIN) * randn(1,N_PART);
    else
        lklhd=lklhd/normalization;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % selection
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    offsprings = f_resample_comb_randshift(lklhd);
    indices    = f_resample_indices(offsprings);

    X1 = X1p(indices);
    X2 = X2p(indices);

    % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    % >>> graph >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    % --- PREDICTION ---

    clf;
    axis([X1_MIN X1_MAX X2_MIN X2_MAX ]);
    axis square;
    hold on;
    % --- the particles
    plot(X1p,X2p,...
        'LineStyle','none','MarkerSize',5,'Marker','x','Color','g');
    % --- mean of the particles
    X1_mean(n) = mean(X1);
    X2_mean(n) = mean(X2);
    plot([X1_MIN X1_MAX],[X2_mean(n) X2_mean(n)],'Color',[0.9 1 0.9]);
    plot([X1_mean(n) X1_mean(n)],[X2_MIN X2_MAX],'Color',[0.9 1 0.9]);
    % plot(X1_mean(1:n),X2_mean(1:n),'g');
    plot(X1_mean(n),X2_mean(n),...
        'LineStyle','none','MarkerSize',5,...
        'Marker','+','Color','g');

    % --- the segments
    for n_seg=1:N_SEGMENTS
        plot([X1A_SEGMENTS(n_seg) X1B_SEGMENTS(n_seg)],...
            [X2A_SEGMENTS(n_seg) X2B_SEGMENTS(n_seg)],...
            'black','LineWidth',2);
    end
    % --- the sites
    plot(X1_SITES,X2_SITES,'gs',...
        'LineStyle','none','MarkerEdgeColor','k',...
        'MarkerFaceColor','w','MarkerSize',10);
    % --- observation "arrows"
    % for n_site = 1:N_SITES
    %  if observation(1,n_site,n) == 1;
    %    plot([X1_SITES(n_site) ...
    %	    X1_SITES(n_site)+2000*cos(pi/2.-observation(2,n_site,n))],...
    %	   [X2_SITES(n_site) ...
    %	    X2_SITES(n_site)+2000*sin(pi/2.-observation(2,n_site,n))],'r');
    %      plot(X1_SITES(n_site),X2_SITES(n_site),'gs',...
    %	   'LineStyle','none','MarkerEdgeColor','k',...
    %	   'MarkerFaceColor','g','MarkerSize',10);
    %   end
    %  end

    plot(X1_TRAJ,X2_TRAJ,'r-');
    % plot(X1_mean(1:n),X2_mean(1:n),'b');
    plot(X1_TRAJ(n),X2_TRAJ(n),'r+');

    drawnow

    answer1 = 'o';
    answer1 = input(['       >>> continue ? y/n [y] '],'s');
    if (answer1 == 'n')
        break;
    end;

    % --- CORRECTION ---

    clf;
    axis([X1_MIN X1_MAX X2_MIN X2_MAX ]);
    axis square;
    hold on;
    % --- the particles
    plot(X1,X2,...
        'LineStyle','none','MarkerSize',5,'Marker','x','Color','b');
    % --- mean of the particles
    X1_mean(n) = mean(X1);
    X2_mean(n) = mean(X2);
    plot([X1_MIN X1_MAX],[X2_mean(n) X2_mean(n)],'Color',[0.9 1 0.9]);
    plot([X1_mean(n) X1_mean(n)],[X2_MIN X2_MAX],'Color',[0.9 1 0.9]);
    % plot(X1_mean(1:n),X2_mean(1:n),'g');
    plot(X1_mean(n),X2_mean(n),...
        'LineStyle','none','MarkerSize',5,...
        'Marker','+','Color','g');

    % --- the segments
    for n_seg=1:N_SEGMENTS
        plot([X1A_SEGMENTS(n_seg) X1B_SEGMENTS(n_seg)],...
            [X2A_SEGMENTS(n_seg) X2B_SEGMENTS(n_seg)],...
            'black','LineWidth',2);
    end

    % --- the sites
    plot(X1_SITES,X2_SITES,'gs',...
        'LineStyle','none','MarkerEdgeColor','k',...
        'MarkerFaceColor','w','MarkerSize',10);
    
    % --- observation "arrows"
    for n_site = 1:N_SITES
        if observation(1,n_site,n) == 1;
            plot([X1_SITES(n_site) ...
                X1_SITES(n_site)+2000*cos(pi/2.-observation(2,n_site,n))],...
                [X2_SITES(n_site) ...
                X2_SITES(n_site)+2000*sin(pi/2.-observation(2,n_site,n))],'r');
            plot(X1_SITES(n_site),X2_SITES(n_site),'gs',...
                'LineStyle','none','MarkerEdgeColor','k',...
                'MarkerFaceColor','g','MarkerSize',10);
        end
    end

    plot(X1_TRAJ,X2_TRAJ,'r-');
    % plot(X1_mean(1:n),X2_mean(1:n),'b');
    plot(X1_TRAJ(n),X2_TRAJ(n),'r+');

    drawnow
    answer1 = 'o';
    answer1 = input(['       >>> Continue ? y/n [y] '],'s');
    if (answer1 == 'n')
        break;
    end;

    % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    % >>> end of the graph >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    % >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

end % end of the time loop



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function res=intersect(a1x,a1y,a2x,a2y,b1x,b1y,b2x,b2y)
%--------------------------------------------------------------------------
%                      _______ _______ _______ _______
%                        A1      A2      B1      B2
% check if the segments A=[A1 A2] and B=[B1 B2] intersect or not
% with Ai=(aix,aiy) Bi=(bix,biy) i=1,2
%   = 1 they intersect
%   = 0 they don't
%--------------------------------------------------------------------------
% a2x a2y can be arrays
%--------------------------------------------------------------------------
% (C) Fabien Campillo INRIA
% created   November 15 2002
% modified  July     25 2003
%--------------------------------------------------------------------------
coteA1_B1B2 = sign((a1x-b1x)*(b2y-b1y)-(a1y-b1y)*(b2x-b1x)) ;
coteA2_B1B2 = sign((a2x-b1x)*(b2y-b1y)-(a2y-b1y)*(b2x-b1x)) ;
coteB1_A1A2 = sign((b1x-a1x)*(a2y-a1y)-(b1y-a1y)*(a2x-a1x)) ;
coteB2_A1A2 = sign((b2x-a1x)*(a2y-a1y)-(b2y-a1y)*(a2x-a1x)) ;

% they intersect if and only if
%   coteA1_B1B2 and coteA2_B1B2 have opposite signs
%   coteB1_A1A2 and coteB2_A1A2 have opposite signs
% i.e. if and only if the following term is -1

% note the use of ".*"
rres=max(coteA1_B1B2.*coteA2_B1B2,coteB1_A1A2.*coteB2_A1A2);

res = zeros(size(a2x));
res(rres==-1)=1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function res=visibles(ax,ay,bx,by, ...
    N_SEGMENTS,X1A_SEGMENTS,X2A_SEGMENTS, ...
    X1B_SEGMENTS,X2B_SEGMENTS)
%--------------------------------------------------------------------------
% check if the points A(ax,ay) and B(bx,by) are visibles one from 
% the other in the segments net
%   = 1 they are visibles
%   = 0 they are not
%--------------------------------------------------------------------------
% bx and by can be arrays
%--------------------------------------------------------------------------
% (C) Fabien Campillo INRIA
% created   November 15 2002
% modified  July     25 2003
%--------------------------------------------------------------------------
res = 1;
for n_seg=1:N_SEGMENTS
    non_intersect = 1-intersect(ax,ay,bx,by, ...
        X1A_SEGMENTS(n_seg),X2A_SEGMENTS(n_seg), ...
        X1B_SEGMENTS(n_seg),X2B_SEGMENTS(n_seg));
    res = res .* non_intersect ;
end



