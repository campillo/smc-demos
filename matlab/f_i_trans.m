%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================



function [i_vois,i_masq] = f_i_trans(i_observee,Npsf)
%-----------------------------------------------------------------------------
% transform a 2D pic to a 1D array....
NNpsf = (2*Npsf+1)*(2*Npsf+1);
[n_px n_py] = size(i_observee);
n_p = n_px*n_py;

i_vois = zeros(n_p,NNpsf); % the value of the pixels
i_masq = zeros(n_p,NNpsf); % masks (edge pb)

indices_2d = reshape(1:n_p,n_px,n_py);

for ii=-Npsf:Npsf
  for jj=-Npsf:Npsf
    
    % --- the neighbors
    z1=ones(n_px,n_py);
    z1(max(1,1+ii):min(n_px,n_px+ii),...
       max(1,1+jj):min(n_py,n_py+jj)) =...
	i_observee(max(1-ii,1):min(n_px-ii,n_px),...
		   max(1-jj,1):min(n_py-jj,n_py));
    b1 = reshape(z1,1,n_p);
    i_vois(:,(ii+Npsf+1)+(2*Npsf+1)*(jj+Npsf)) = b1(:);
    
    % --- the mask
    z0=zeros(n_px,n_py);
    z0(max(1,1+ii):min(n_px,n_px+ii),...
       max(1,1+jj):min(n_py,n_py+jj)) =...
	indices_2d(max(1-ii,1):min(n_px-ii,n_px),...
		   max(1-jj,1):min(n_py-jj,n_py));
    b0 = reshape((z0>0),1,n_p);
    i_masq(:,(ii+Npsf+1)+(2*Npsf+1)*(jj+Npsf)) = b0(:);
    
  end
end
