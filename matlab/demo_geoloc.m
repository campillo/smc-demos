%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function demo_geoloc()
%==========================================================================
% Positioning using wireless communication network measurements : 
% A mobile phone is moving in Manhattan: in the streets and in the
% building (Manhattan is just a sugar cube shape geometry of
% regular streets and buildings). The mobile is communicating with
% different stations. For each station a digital map of signal
% strength is provided. WE SUPPOSE that the strength of the signal
% is very low when the mobile is inside a building. The received
% signal strength (RSS) is measured, the aim is to locate the
% mobile.
%
% The geometry of the problem (number of buildings, street length,
% building length) could be provided by the user. In this case the
% digital maps of signal strength are simulated. 
%
% For the signal strenghth attenuation we use the formula proposed
% in: Masaharu Hata. Empirical formula for propagation loss in land
%     mobile radio services. IEEE Transactions on Vehicular Technology,
%     VT-29(3):317-325, 1980. 
%==========================================================================
% Version 1 : demo_geoloc.m
%     created on 16 Octobre 2004
%     - modified 21 Octobre 2004
%     - March 11, 2009: cleaning and English
%==========================================================================

global X1 X2

disp(' ');
disp(' ');
disp(' --- geolocation ------------------------------------------------------');
disp(' ');
disp('     1 - scenario ');
%==============================================================================
answer1 = 'n';
if exist('demo_geoloc.mat')~=0
  answer1 = input(['\n         >>> a scenario already exists: ' ...
                   'keep it? y/n [y] '],'s');
  if isempty(answer1)
    answer1 = 'y'; 
  end
end
%==============================================================================

if answer1 == 'n'

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % --- THE PARAMETER
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % -------------------------------------------------------------------------
  % Main parameters 
  % -------------------------------------------------------------------------
  % --- Geometry and signal strength    
  Ncell      = 5;     % number of cells
  x1_min     = 10;    % lower-left corner coordinates of the real map
  x2_min     = 100;   %     (used by the function rectangle)
  DELTA      = 200;   % side size of the map (the map is squared)
  Nb_pix_build = 25;  % number of pixels for a building
  Nb_pix_street = 2;  % number of pixels for the street (even number !!!)
  P_t        = 120;   % transmited power [db]
  alpha      = 2;     % signal attenuation coefficient 
  K          = 2;     % coefficient K
  sigma_stat = 0.2;   % standard deviation of the noise for the map
                      % associated with each station (noise ~ N(0,sigma_st^2))
  
  
  % example of Manhattan : 3 x 3 cells 
  %       --------------------------------
  %       l                              l
  %       l  ------    ------    ------  l
  %       l  l    l    l    l    l    l  l
  %       l  l    l    l    l    l    l  l
  %       l  ------    ------    ------  l
  %       l                              l
  %       l  ------    ------    ------  l
  %       l  l    l    l    l    l    l  l
  %       l  l    l    l    l    l    l  l
  %       l  ------    ------    ------  l
  %       l                              l
  %       l  ------    ------    ------  l
  %       l  l    l    l    l    l    l  l
  %       l  l    l    l    l    l    l  l
  %       l  ------    ------    ------  l
  %       l                              l
  %       l-------------------------------
  
  
  % --- simulation
  speed    = 2;       % displacement speed of the mobile (a
                      % pedestrian) meter/sec 
  dt         = 1;     % observation time step (frequency of obserbation)
  sigma_v    = 0.6;   % observation noise standard deviation
                      % (noise ~ N(0,sigma_v^2))
  Nb_step_min = 5;    % minimal number of time steps by
                      % displacement segment 
  
  % --- Filter
  Nb_part    = 5000;  % number of particles
  sigma_w    = 3;     % standard devation of the model noise
                      % (noise ~ N(0,sigma_w^2))
  delta_x  = 40;      % 1/2 side of the square for the initialisation
  
  % --- Graph
  marge             = DELTA*0.05;
  line_border       = 2;
  facecolor_build   = [0.75,0.75,0.75];
  edgecolor_build   = [0.35,0.35,0.35];
  edgecolor_red     = [1 0 0];
  color_stat        = 'g';
  markersize_stat   = 15;
  markersize_pedestrian = 15;
  markersize_part   = 1;
  
  % ----------------------------------------------------------------------------
  % computation of the secondary parameters
  % ----------------------------------------------------------------------------
  x1_max      = x1_min + DELTA;          % upper-right coner coordinates 
  x2_max      = x2_min + DELTA;          %         on the psysical map
  Nb_pix_cell = Nb_pix_build + Nb_pix_street; % nb of pixels for a cell
  Nb_pixel    = Ncell * Nb_pix_cell;     % nb of pixels in a direction 
                                         %         for the pixel map
  delta     = DELTA/Ncell;               % side size of the cell 
  delta_build = delta*Nb_pix_build/Nb_pix_cell; % side size of the building
  delta_street = delta*Nb_pix_street/Nb_pix_cell; % width of the street 
  
  % [px1,px2] = meshgrid(1:Nb_pixel,1:Nb_pixel);
  % [X1 X2]=f_pixel_to_space(px1,px2,x1_min,x1_max, ...
  %    x2_min,x2_max,Nb_pixel,Nb_pixel);
  
  % ----------------------------------------------------------------------------
  % Graphical user interface 
  % ----------------------------------------------------------------------------

  % Computing and displaying the real map
  % ----------------------------------------------------------------------------
  clf;       
  rectangle('Position',[x1_min,x2_min,DELTA,DELTA]);    
  daspect([1,1,1]);
  axis([x1_min-marge, x1_max+marge,x2_min-marge, x2_max+marge]);
  for j = 1:Ncell
    x2 = x2_min + delta_street*0.5 + (j-1)*delta;
    for i = 1:Ncell
      x1 = x1_min + delta_street*0.5 + (i-1)*delta;                
      rectangle('Position',[x1,x2,delta_build,delta_build], ...
                'FaceColor',facecolor_build,'EdgeColor',edgecolor_build);
    end;
  end;
  % ----------------------------------------------------------------------------
  [px1,px2] = meshgrid(1:Nb_pixel,1:Nb_pixel);
  [X1 X2]   = f_pixel_to_space(px1,px2,x1_min,x1_max, ...
                               x2_min,x2_max,Nb_pixel,Nb_pixel);
  
  % placing the different measurement stations
  % ----------------------------------------------------------------------------
  for i = 1:Ncell+1
    x1 = x1_min + (i-1)*delta;
    for j = 1:Ncell+1
      x2 = x2_min + (j-1)*delta;
      x1_crossroad(i,j) = x1;
      x2_crossroad(i,j) = x2;
    end;
  end;
  Goal = 1;
  Nb_stat = 0;    
  coord_station = [];
  disp('         positionning the measurement stations')
  disp('         (station are at crossroads)')
  disp('         (left-click for positionning a station and right-click to finish)');
  while Goal == 1
    [x1, x2, Goal]=ginput(1);
    res_test = test_station(x1,x2,Ncell,x1_min,x2_min,x1_max,x2_max,delta);
    if (res_test == 1)
      Nb_stat = Nb_stat + 1;
      abscisse    =  (x1 >= x1_crossroad - 0.5*delta) & ...
          (x1 <= x1_crossroad + 0.5*delta);
      ordonnee    =  (x2 >= x2_crossroad - 0.5*delta) & ...
          (x2 <= x2_crossroad + 0.5*delta);
      mask = abscisse & ordonnee;
      x1 = x1_crossroad(mask ~= 0);
      x2 = x2_crossroad(mask ~= 0);
      coord_station(1,Nb_stat) = x1;
      coord_station(2,Nb_stat) = x2;
      hold on;
      plot(x1,x2,[color_stat '.'],'MarkerSize',markersize_stat);
      disp(['         station ',num2str(Nb_stat)]);
      drawnow;
    else
      disp('         warning : click near a crossroads');
      Goal = 1;
    end;
  end;      
  
  % ============================================================================
  % Attenuation maps computation
  % ============================================================================
  
  disp(' ');
  disp('     2 - attenuation maps computation');
  disp(' ');
  
  % mask for the map 
  % ----------------------------------------------------------------------------
  mask_map = ones(Nb_pixel,Nb_pixel);
  n = 1;
  for i = 1:Nb_pixel
    m=1;
    if (n > Nb_pix_street*0.5) & (n <= (Nb_pix_street*0.5+Nb_pix_build))
      for j = 1:Nb_pixel
        if (m > Nb_pix_street*0.5) & (m <= (Nb_pix_street*0.5+Nb_pix_build))
          mask_map(i,j) = 0;
          m = m+1;
        else
          if (m < Nb_pix_cell)
            m = m+1;
          else
            m = 1;
          end;
        end;
      end;
      n = n+1;
    else
      if (n < Nb_pix_cell)
        n = n+1;
      else
        n = 1;
      end;
    end;
  end;
  
  % attenuation maps  for each station
  
  % ----------------------------------------------------------------------------
  all_map_station = zeros(Nb_pixel,Nb_pixel,Nb_stat);
  for s=1:Nb_stat        
    x1_stat = coord_station(1,s);
    x2_stat = coord_station(2,s);      
    map_stat = create_map_stat(x1_stat,x2_stat,P_t,alpha,K,Ncell, ...
          x1_min,x2_min,x1_max,x2_max,Nb_pixel,Nb_pix_cell,Nb_pix_street,...
          delta,delta_street,mask_map);
    bruit_stat = sigma_stat*randn(Nb_pixel,Nb_pixel);
    map_noisy = (map_stat + bruit_stat).*mask_map;
    all_map_station(:,:,s) = map_noisy; 
    disp(['         map ',num2str(s), ' done']);          
  end;
  
  % ============================================================================
  % simulating the trajectory of the mobile (pedestrian)
  % ============================================================================

  disp(' ');
  disp('     3 - pedestrian (mobile) trajectory');
  disp(' ');
  
  Goal = 1;
  Nb_point = 0;    
  disp(['         left-click and right-click to end the trajectory']);
  while Goal == 1    
    [x1, x2, Goal]=ginput(1);    
    
    res_test = ( (x1 >= x1_min) & (x1 <= x1_max) & ...
                 (x2 >= x2_min) & (x2 <= x2_max) );    
    
    if (res_test == 1)
      Nb_point = Nb_point + 1;
      hold on;
      plot(x1,x2,'b+')
      x1_traj(Nb_point) = x1;
      x2_traj(Nb_point) = x2;
    else
      disp('         warning: click inside the map!');
      Goal = 1;
    end;        
    if (Goal == 3) & (Nb_point == 1)
      Goal = 1;
      disp('         warning: at least 1 destination point');
    end;        
  end;
  
  Nb_obs = 1;
  d_step  = speed * dt;
  Longueur_traj = 0;
  if (Nb_point >= 2)
    
    % 3 possibilities
    
    % constant speed displacement on each segment:
    %
    % [x1_traj_fit, x2_traj_fit] = interp_1(x1_traj, x2_traj,speed,dt);
    % [nn Nb_obs] = size(x1_traj_fit);    
    
    % different speed displacement on each segment, but:
    %    - given maximum speed on long segments 
    %    - slow speed on short segments (the segment is made with a
    %      minimum of Nb_step_min)
    %
    [x1_traj_fit, x2_traj_fit] = interp_2(x1_traj, x2_traj, ...
                                          speed,dt,Nb_step_min);
    [nn Nb_obs] = size(x1_traj_fit);    
    
    %  different speed displacement on each segment, but:
    %    - high speed on long segments 
    %    - slow speed on short segments
    %
    % [x1_traj_fit, x2_traj_fit] = interp_3(x1_traj, x2_traj,speed,dt);
    % [nn Nb_obs] = size(x1_traj_fit);    
    
    
    % Redraw the map with the stations -------------------------------------
    clf;       
    rectangle('Position',[x1_min,x2_min,DELTA,DELTA]);    
    daspect([1,1,1]);
    axis([x1_min-marge, x1_max+marge,x2_min-marge, x2_max+marge]);
    for j = 1:Ncell
      x2 = x2_min + delta_street*0.5 + (j-1)*delta;
      for i = 1:Ncell
        x1 = x1_min + delta_street*0.5 + (i-1)*delta;                
        rectangle('Position',[x1,x2,delta_build,delta_build], ...
                  'FaceColor',facecolor_build,'EdgeColor',edgecolor_build);
      end;
    end;
    hold on;
    for s=1:Nb_stat
      plot(coord_station(1,s), coord_station(2,s),...
           [color_stat '.'],'MarkerSize',markersize_stat);
    end;
    
    hold on;    
    for i=1:Nb_obs       
      plot(x1_traj_fit(i),x2_traj_fit(i),'b.',...
           'MarkerSize',1);    
    end;       
    drawnow;
    coord_pedestrian(1,:) = x1_traj_fit;
    coord_pedestrian(2,:) = x2_traj_fit; 
  end;
  
  % lower left corner coordinates of the square of initialisation for
  % the filter (the initial position law is uniform on a square)
  x1_init  = coord_pedestrian(1,1) - delta_x;
  x2_init  = coord_pedestrian(2,1) - delta_x;
  
  % ----------------------------------------------------------------------------
  %  Saving workspace 
  % ----------------------------------------------------------------------------
  save('demo_geoloc.mat');
  
else

  load('demo_geoloc.mat');
  clf;       
  rectangle('Position',[x1_min,x2_min,DELTA,DELTA]);    
  daspect([1,1,1]);
  axis([x1_min-marge, x1_max+marge,x2_min-marge, x2_max+marge]);
  for j = 1:Ncell
    x2 = x2_min + delta_street*0.5 + (j-1)*delta;
    for i = 1:Ncell
      x1 = x1_min + delta_street*0.5 + (i-1)*delta;                
      rectangle('Position',[x1,x2,delta_build,delta_build], ...
                'FaceColor',facecolor_build,'EdgeColor',edgecolor_build);
    end;
  end;
  hold on;
  for i= 1:Nb_stat
    plot(coord_station(1,i), coord_station(2,i),...
         [color_stat '.'],'MarkerSize',markersize_stat);
  end; 
  for i=1:Nb_obs       
    plot(coord_pedestrian(1,i),coord_pedestrian(2,i),'b.', ...
         'MarkerSize',1);  
  end;       
  drawnow;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% displaying the attenuation maps with the stations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
answer1 = 'n';
disp(' ');
disp('     4 - Displaying the attenuation maps');
disp(' ');
answer1 = input(['          >>>  Displaying the attenuation maps '...
                 '? y/n [n] '],'s');
if (answer1 == 'y')
  h = gcf;    
  for s=1:Nb_stat
    figure;
    surf(X1, X2, all_map_station(:,:,s));
    axis xy
    shading interp;        
  end;    
  figure(h);
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% measurements simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('     5 - measurements simulation');
disp(' ');

disp('         press ENTER to simulate observations ');
pause
    

obs_pedestrian = [];
pixel_pedestrian_1 = zeros(1,Nb_obs);  
pixel_pedestrian_2 = zeros(1,Nb_obs);  
[pixel_pedestrian_1, pixel_pedestrian_2] = ...
    f_space_to_pixel(coord_pedestrian(1,:),coord_pedestrian(2,:), ...
    x1_min,x1_max,x2_min,x2_max,Nb_pixel,Nb_pixel);

the_observations  = zeros(Nb_stat,Nb_obs);    
for s = 1:Nb_stat
    for k = 1:Nb_obs    
        the_observations (s,k) = all_map_station(pixel_pedestrian_1(k), ...
            pixel_pedestrian_2(k), s);
    end;
end;
bruit_obs = sigma_v*randn(Nb_stat,Nb_obs);
obs_pedestrian = the_observations + bruit_obs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- PARTICLE FILTERING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- memory allocation ---
H_xsi = ones(Nb_stat,Nb_part);
% ---

disp(' ');
disp('     6 - filtering');
disp(' ');

disp('         initialization');

% Initialization of the particle system (uniform law on a square)
xsi_0_x1 = x1_init + 2*delta_x*rand(1,Nb_part);
xsi_0_x2 = x2_init + 2*delta_x*rand(1,Nb_part);

hold on;
plot(xsi_0_x1(1:Nb_part),xsi_0_x2(1:Nb_part),'r.',...
     'MarkerSize',markersize_part); 

set(gcf,'DoubleBuffer','on');
drawnow;

disp('         [enter to continue]...');
pause

% particle filtering
weights = zeros(1,Nb_part);
xsi_k_1_x1  = xsi_0_x1;  
xsi_k_1_x2  = xsi_0_x2; 

%nb_grand = realmax;

H_xsi = zeros(Nb_pixel,Nb_pixel,Nb_stat);
Y_k   = zeros(Nb_pixel,Nb_pixel,Nb_stat);
DH    = zeros(Nb_pixel,Nb_pixel);
[Nb_lg_DH, Nb_col_DH] = size(DH);
indices = 1:Nb_part;    

for k=1:Nb_obs

  % Prediction --------------------------------------------------------------
  xsi_k_x1 = xsi_k_1_x1 + sigma_w*randn(1,Nb_part);
  xsi_k_x2 = xsi_k_1_x2 + sigma_w*randn(1,Nb_part);
  
  % Correction --------------------------------------------------------------
  [pix1, pix2] = ...
      f_space_to_pixel(xsi_k_x1,xsi_k_x2,x1_min,x1_max,...
                       x2_min,x2_max,Nb_pixel,Nb_pixel);
  
  mask = (pix1>=1) & (pix1<=Nb_pixel) & (pix2>=1) & (pix2<=Nb_pixel);
  
  for s = 1:Nb_stat        
    Y_k(:,:,s) = obs_pedestrian(s,k);
  end;   
  
  H_xsi = all_map_station - Y_k;       
  DH    = exp( -0.5/sigma_v^2 *sum(H_xsi.*H_xsi,3) );    
  
  weights_k = zeros(1,Nb_part);
  pix1  = pix1.*mask;
  pix2  = pix2.*mask;
  for i=1:Nb_part
    if (pix1(i) ~= 0)
      weights_k(i) = DH(pix1(i),pix2(i)); 
    end;       
  end;
  
  % particle redistribution -------------------------------------------    
  cte_norm = sum(weights_k);
  if (cte_norm == 0)
    weights_k = ones(1,Nb_part)/Nb_part;
    disp('               ! null likelihood ');
  else
    weights_k = weights_k/cte_norm;
    offsprings = f_resample_residual(weights_k);
    indices    = f_resample_indices(offsprings);
    xsi_k_x1   = xsi_k_x1(indices);
    xsi_k_x2   = xsi_k_x2(indices);
  end;
  xsi_k_1_x1  = xsi_k_x1;
  xsi_k_1_x2  = xsi_k_x2;
  
  % redraw the map with the stations -------------------------------------
  clf;       
  rectangle('Position',[x1_min,x2_min,DELTA,DELTA]);    
  daspect([1,1,1]);
  axis([x1_min-marge, x1_max+marge,x2_min-marge, x2_max+marge]);
  for j = 1:Ncell
    x2 = x2_min + delta_street*0.5 + (j-1)*delta;
    for i = 1:Ncell
      x1 = x1_min + delta_street*0.5 + (i-1)*delta;                
      rectangle('Position',[x1,x2,delta_build,delta_build], ...
                'FaceColor',facecolor_build,'EdgeColor',edgecolor_build);
    end;
  end;
  hold on;
  for s=1:Nb_stat
    plot(coord_station(1,s), coord_station(2,s),...
         [color_stat '.'],'MarkerSize',markersize_stat);
  end;
  
  plot(xsi_k_x1(1:Nb_part),xsi_k_x2(1:Nb_part),'r.',...
       'MarkerSize',markersize_part); 
  
  plot(coord_pedestrian(1,k),coord_pedestrian(2,k),'b.',...
       'MarkerSize',markersize_pedestrian);            
  
  set(gcf,'DoubleBuffer','on');
  drawnow;                                     
end;    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%  AUXILIARY FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function res = test_station(x1_s, x2_s, Ncell, x1_min, x2_min, ...
			      x1_max, x2_max, delta); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check if the coordinates (x1_s, x2_s) belong to the neighborhood
% of a crossraod
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
res = [];
for i = 1:Ncell+1
  x1_car = x1_min + (i-1)*delta;
  if (x1_car == x1_min)
    for j = 1:Ncell+1
      x2_car = x2_min + (j-1)*delta;
      if (x2_car == x2_min)
        res = [res (x1_s>=x1_min) & (x1_s<=x1_car+delta/3) & ...
               (x2_s>=x2_min) & (x2_s<=x2_car+delta/3)];
      elseif (x2_car == x2_max)
        res = [res (x1_s>=x1_min) & (x1_s<=x1_car+delta/3) & ...
               (x2_s>=x2_car-delta/3) & (x2_s <= x2_max)];
      else
        res = [res (x1_s>=x1_min) & (x1_s<=x1_car+delta/3) & ...
               (x2_s>=x2_car-delta/3) & (x2_s<=x2_car+delta/3)];
      end;
    end;
  elseif (x1_car == x1_max)
    for j = 1:Ncell+1
      x2_car = x2_min + (j-1)*delta;
      if (x2_car == x2_min)
        res = [res (x1_s>=x1_car-delta/3) & (x1_s<=x1_max) & ...
               (x2_s>=x2_min) & (x2_s<=x2_car+delta/3)];
      elseif (x2_car == x2_max)
        res = [res (x1_s>=x1_car-delta/3) & (x1_s<=x1_max) & ...
               (x2_s>=x2_car-delta/3) & (x2_s <= x2_max)];
      else
        res = [res (x1_s>=x1_car-delta/3) & (x1_s<=x1_max) & ...
               (x2_s>=x2_car-delta/3) & (x2_s<=x2_car+delta/3)];
      end;
    end;
  else
    for j = 1:Ncell+1
      x2_car = x2_min + (j-1)*delta;
      if (x2_car == x2_min)
        res = [res (x1_s>=x1_car-delta/3) & ...
               (x1_s<=x1_car+delta/3) & ...
               (x2_s>=x2_min) & (x2_s<=x2_car+delta/3)];
      elseif (x2_car == x2_max)
        res = [res (x1_s>=x1_car-delta/3) & ...
               (x1_s<=x1_car+delta/3) & ...
               (x2_s>=x2_car-delta/3) & (x2_s <= x2_max)];
      else
        res = [res (x1_s>=x1_car-delta/3) & ...
               (x1_s<=x1_car+delta/3) & ...
               (x2_s>=x2_car-delta/3) & (x2_s<=x2_car+delta/3)];
      end;
    end;
  end;
end;
res = res(res == 1);
if isempty(res)
  res = 0;
end;



function une_map_stat = create_map_stat(x1_s,x2_s,P_t,alpha, K, ...
    Ncell,x1_min,x2_min,x1_max,x2_max,Nb_pixel,Nb_pix_cell,Nb_pix_street,...
    delta,delta_street,mask_map);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create the attenuation map
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global X1 X2;
    
% --- associated line of sight zone --------------------------------------------

% mask for the LOS (line of sight)   

masque_LOS = zeros(Nb_pixel);
masque_LOS( (abs(X1-x1_s) < (delta_street*0.5)) | ...
            (abs(X2-x2_s) < (delta_street*0.5)) ) = 1;

% compute the distances from the pixels to the concerned station

map_LOS = distance(x1_s,x2_s,X1,X2);
map_LOS(masque_LOS == 0) = realmax;

% --- Zone NLOS associ?e -------------------------------------------------------

% Calcul du masque pour la zone NLOS    

masque_NLOS = mask_map .* not(masque_LOS);

% compute the number of streets in the NLOS zone where: 
%   x1 coordinates: X1 < x1_s,  X1 > x1_s,
%   x2 coordinates: X2 < x2_s,  X2 > x2_s

Nb_x1_inf = round((x1_s - x1_min)/delta);
Nb_x1_sup = round((x1_max - x1_s)/delta);
Nb_x2_inf = round((x2_s - x2_min)/delta);
Nb_x2_sup = round((x2_max - x2_s)/delta);

% mask for the computation of the distances for each street
all_NLOS = zeros(Nb_pixel,Nb_pixel,4*Ncell);

cpt = 1;        

% for the street on the x1-axis of X1 < x1_s    
for i=1:Nb_x1_inf
  NLOS_street = zeros(Nb_pixel,Nb_pixel);
  if (i == 1)
    mask= (X1 > x1_min) & ...      
          (X1 < x1_min + (delta/Nb_pix_cell)*Nb_pix_street*0.5);
    NLOS_street(mask) = 1;
  else
    mask = (X1 > (x1_min + (i-1)*delta - ...
                  (delta/Nb_pix_cell)*Nb_pix_street*0.5) ) & ...
           (X1 < (x1_min + (i-1)*delta + ...
                  (delta/Nb_pix_cell)*Nb_pix_street*0.5) );
    NLOS_street(mask) = 1;
  end;   
  % determine the point coordinates of the street where we compute    
  all_NLOS(:,:,cpt)   = NLOS_street;    
  all_NLOS(:,:,cpt+1) = NLOS_street;
  cpt = cpt + 2;        
end;        
% for the streets on the axis X1 > x1_s
for i=(Nb_x1_inf+1):Ncell
  NLOS_street = zeros(Nb_pixel,Nb_pixel);
  if (i == Ncell)
    mask = (X1 > (x1_min + i*delta - ...
                  (delta/Nb_pix_cell)*Nb_pix_street*0.5) ) & (X1 < x1_max);
    NLOS_street(mask) = 1;
  else
    mask = ( X1 > (x1_min + i*delta - ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) ) & ...
           ( X1 < (x1_min + i*delta + ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) );
    NLOS_street(mask) = 1;
  end;
  % determine the point coordinates of the street where we compute
  all_NLOS(:,:,cpt)   = NLOS_street;
  all_NLOS(:,:,cpt+1) =  NLOS_street;    
  cpt = cpt + 2;              
end;    
% for the streets on the axis X2 < x2_s  
for i=1:Nb_x2_inf        
  NLOS_street = zeros(Nb_pixel,Nb_pixel);
  if (i == 1)
    mask = (X2 > x2_min) & (X2 < x2_min + ...
                            (delta/Nb_pix_cell)*Nb_pix_street*0.5);
    NLOS_street(mask) = 1;
  else
    mask = ( X2 > (x2_min + (i-1)*delta - ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) ) & ...
           ( X2 < (x2_min + (i-1)*delta + ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) );
    NLOS_street(mask) = 1;
  end;
  % determine the point coordinates of the street where we compute
  all_NLOS(:,:,cpt)   = NLOS_street;
  all_NLOS(:,:,cpt+1) = NLOS_street;    
  cpt = cpt + 2;        
end;        
% for the streets on the axis X2 > x2_s
for i=(Nb_x2_inf+1):Ncell
  NLOS_street = zeros(Nb_pixel,Nb_pixel);
  if (i == Ncell)
    mask = ( X2 > (x2_min + i*delta - ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) ) & (X2 < x2_max);
    NLOS_street(mask) = 1;
  else
    mask = ( X2 > (x2_min + i*delta - ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) ) & ...
           ( X2 < (x2_min + i*delta + ...
                   (delta/Nb_pix_cell)*Nb_pix_street*0.5) );
    NLOS_street(mask) = 1;
  end;
  % determine the point coordinates of the street where we compute
  all_NLOS(:,:,cpt)   = NLOS_street;
  all_NLOS(:,:,cpt+1) = NLOS_street;
  cpt = cpt + 2;                
end;
cpt = cpt-1;

% Coordinates of the building corners for the horizontal streets
if (Nb_x2_inf ~= 0) & (Nb_x2_inf ~= Ncell)
  corner_build = [ x2_min + Nb_x2_inf*delta - delta/Nb_pix_cell , ...
                   x2_min + Nb_x2_inf*delta + delta/Nb_pix_cell ];
elseif (Nb_x2_inf ~= Ncell)
  corner_build = x2_min + delta/Nb_pix_cell;
else
  corner_build = x2_max - delta/Nb_pix_cell;
end;
nb_corner = length(corner_build);
for i=1:Nb_x1_inf
  if (i==1)
    x1_horiz(1,i) = x1_min + delta/Nb_pix_cell;
    x2_horiz(1,i) = corner_build(1);
    if (nb_corner > 1)
      x1_horiz(2,i) = x1_min + delta/Nb_pix_cell;
      x2_horiz(2,i) = corner_build(2);
    end;
  else
    x1_horiz(1,i) = x1_min + (i-1)*delta + delta/Nb_pix_cell;
    x2_horiz(1,i) = corner_build(1);
    if (nb_corner > 1)
      x1_horiz(2,i) = x1_min + (i-1)*delta + delta/Nb_pix_cell;
      x2_horiz(2,i) = corner_build(2);
    end;            
  end;
end;    
for i=Nb_x1_inf+1:Ncell
  if (i==Ncell)
    x1_horiz(1,i) = x1_max - delta/Nb_pix_cell;
    x2_horiz(1,i) = corner_build(1);
    if (nb_corner > 1)
      x1_horiz(2,i) = x1_max - delta/Nb_pix_cell;
      x2_horiz(2,i) = corner_build(2);
    end;
  else    
    x1_horiz(1,i) = x1_min + i*delta - delta/Nb_pix_cell;
    x2_horiz(1,i) = corner_build(1);
    if (nb_corner > 1)
      x1_horiz(2,i) = x1_min + i*delta - delta/Nb_pix_cell;
      x2_horiz(2,i) = corner_build(2);
    end;
  end; 
end;

% Coordinates of the building corners for the vertical streets
if (Nb_x1_inf ~= 0) & (Nb_x1_inf ~= Ncell)
  corner_build = [ x1_min + Nb_x1_inf*delta - delta/Nb_pix_cell , ...
                   x1_min + Nb_x1_inf*delta + delta/Nb_pix_cell ];
elseif (Nb_x1_inf ~= Ncell)
  corner_build = x1_min + delta/Nb_pix_cell;
else
  corner_build = x1_max - delta/Nb_pix_cell;
end;
nb_corner = length(corner_build);
for i=1:Nb_x2_inf
  if (i==1)
    x2_vert(1,i) = x2_min + delta/Nb_pix_cell;
    x1_vert(1,i) = corner_build(1);
    if (nb_corner > 1)
      x2_vert(2,i) = x2_min + delta/Nb_pix_cell;
      x1_vert(2,i) = corner_build(2);
    end;
  else
    x2_vert(1,i) = x2_min + (i-1)*delta + delta/Nb_pix_cell;
    x1_vert(1,i) = corner_build(1);
    if (nb_corner > 1)
      x2_vert(2,i) = x2_min + (i-1)*delta + delta/Nb_pix_cell;
      x1_vert(2,i) = corner_build(2);
    end;
  end;
end;   
for i=Nb_x2_inf+1:Ncell
  if (i==Ncell)
    x2_vert(1,i) = x2_max - delta/Nb_pix_cell;
    x1_vert(1,i) = corner_build(1);
    if (nb_corner > 1)
      x2_vert(2,i) = x2_max - delta/Nb_pix_cell;
      x1_vert(2,i) = corner_build(2);
    end;
  else
    x2_vert(1,i) = x2_min + i*delta - delta/Nb_pix_cell;
    x1_vert(1,i) = corner_build(1);
    if (nb_corner > 1)
      x2_vert(2,i) = x2_min + i*delta - delta/Nb_pix_cell;
      x1_vert(2,i) = corner_build(2);
    end;            
  end;        
end;    
% Distance from the station to the corners of the buildings
d_horiz = distance(x1_s, x2_s, x1_horiz, x2_horiz);
d_vert  = distance(x1_s, x2_s, x1_vert , x2_vert );    

% Distance from the station to each street in the NLOS zone 
dist_NLOS = zeros(Nb_pixel,Nb_pixel,4*Ncell);
i = 1;       
while i<=cpt        
  [nb_lg nb_build] = size(x1_horiz);        
  for j=1:nb_build                               
    dist_tmp = distance(x1_horiz(1,j),x2_horiz(1,j),X1,X2);        
    dist_tmp(all_NLOS(:,:,i) == 0) = realmax;
    dist_NLOS(:,:,i) = dist_tmp + d_horiz(1,j);
    i = i+1;
    if (nb_lg > 1)
      dist_tmp = distance(x1_horiz(2,j),x2_horiz(2,j),X1,X2);            
      dist_tmp(all_NLOS(:,:,i) == 0) = realmax;
      dist_NLOS(:,:,i) = dist_tmp + d_horiz(2,j);
      i = i+1;
    else
      dist_tmp = distance(x1_horiz(1,j),x2_horiz(1,j),X1,X2);            
      dist_tmp(all_NLOS(:,:,i) == 0) = realmax;
      dist_NLOS(:,:,i) = dist_tmp + d_horiz(1,j);
      i = i+1;                
    end;
  end;
  
  [nb_lg nb_build] = size(x1_vert);        
  for j=1:nb_build  
    dist_tmp = distance(x1_vert(1,j),x2_vert(1,j),X1,X2);
    dist_tmp(all_NLOS(:,:,i) == 0) = realmax;
    dist_NLOS(:,:,i) = dist_tmp + d_vert(1,j); 
    i = i+1;
    if (nb_lg > 1)            
      dist_tmp = distance(x1_vert(2,j),x2_vert(2,j),X1,X2);
      dist_tmp(all_NLOS(:,:,i) == 0) = realmax;
      dist_NLOS(:,:,i) = dist_tmp  + d_vert(2,j); 
      i = i+1;
    else            
      dist_tmp = distance(x1_vert(1,j),x2_vert(1,j),X1,X2);
      dist_tmp(all_NLOS(:,:,i) == 0) = realmax;
      dist_NLOS(:,:,i) = dist_tmp  + d_vert(1,j);
      i = i+1;                
    end;
  end;                                                   
end;
% Create the digital map associated to the station
map_NLOS = min(dist_NLOS,[],3);               
dist_station = min(map_LOS,map_NLOS);

% Compute the strength of the received signal
une_map_stat = received_signal(P_t,alpha,K,dist_station);   
une_map_stat(isinf(une_map_stat)) = 0;                  





function D = distance(x_init,y_init,x_final,y_final);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the euclidian distance from the station to the point with
% coordinates X,Y 
% Parameters :
%    (x_init,y_init)   : coordinates of the initial point
%    (x_final,y_final) : coordinates of the final point
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D = sqrt( (x_final - x_init).^2 + (y_final - y_init).^2);




function P_r = received_signal(P_t,alpha,K,dist);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the strength of the received signal in terms of the distance "d"
% of the mobile to the station
% Parameters:
%   P_t   : power of the emitted signal from the station
%   alpha : attenuation factor
%   d     : Euclidian distance from the station to the mobile
%   K     : a constant
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elem_neg_d = dist(dist<0);
if length(elem_neg_d) > 0
  un_elem = elem_neg_d(1);
  error(['one of the distances d(.) = ' num2str(un_elem) ' < 0']);
else
  P_r = P_t - 10*alpha*log10(dist/K);    
end;



function [px,py]=f_space_to_pixel(x,y,Xmin,Xmax,Ymin,Ymax,Npx,Npy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [px,py]=f_space_to_pixel(x,y,Xmin,Xmax,Ymin,Ymax,Npx,Npy)
% from space coordinates to pixel coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delta_x = (Xmax-Xmin)/Npx;
delta_y = (Ymax-Ymin)/Npy;
px = round((x-Xmin)./delta_x+0.5);
py = round((y-Ymin)./delta_y+0.5);


function [rx,ry]=f_pixel_to_space(px,py,Xmin,Xmax,Ymin,Ymax,Npx,Npy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [rx,ry]=f_pixel_to_space(px,py,Xmin,Xmax,Ymin,Ymax,Npx,Npy)
% from pixel coordinates to space coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delta_x = (Xmax-Xmin)/Npx;
delta_y = (Ymax-Ymin)/Npy;
rx = Xmin+delta_x*(px-0.5);
ry = Ymin+delta_y*(py-0.5);


function [p1, p2] = interp_1(x1,x2,speed,dt);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the pedestrian trajectory of the segment x1-x2
% 1st possibility
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[nn K] = size(x1);
delta  = speed * dt;
p      = 1;
p1(p)  = x1(p);
p2(p)  = x2(p);
reste  = 0;

for k = 1:K-1
  d     =  sqrt( (x1(k+1)-x1(k))^2 + (x2(k+1)-x2(k))^2. );  
  alpha = atan2( (x2(k+1)-x2(k)),(x1(k+1)-x1(k)) );
  np    = floor(d/delta);
  nnp = 1;
  while (nnp <= np)
    p = p+1;
    p1(p)  = x1(k) + (nnp*delta - reste) * cos(alpha);
    p2(p)  = x2(k) + (nnp*delta - reste) * sin(alpha);
    nnp = nnp + 1;
    
  end;
  if (nnp > np)
    reste = 0;
  end;
  reste = (d/delta - np)*delta; + reste;
end;



function [p1, p2] = interp_2(x1,x2,speed,dt,NL);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the pedestrian trajectory of the segment x1-x2
% 2nd possibility
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[nn K]     = size(x1);
delta_max  = speed * dt;
p          = 1;
p1(p)      = x1(p);
p2(p)      = x2(p);
reste      = 0;

for k = 1:K-1
  L        = sqrt( (x1(k+1)-x1(k))^2 + (x2(k+1)-x2(k))^2. );
  delta    = min(delta_max,L/NL);
  alpha    = atan2( (x2(k+1)-x2(k)),(x1(k+1)-x1(k)) );
  m        = max(NL,floor(L/delta_max));
  for i = 0:m-1       
    p1(p)  = x1(k) + i*delta*cos(alpha);
    p2(p)  = x2(k) + i*delta*sin(alpha);
    p = p + 1;
  end;
end;


function [p1, p2] = interp_3(x1_traj,x2_traj,speed,dt);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the pedestrian trajectory of the segment x1-x2
% 3rd possibility
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
d_step         = speed * dt;
[Nn Nb_point] = size(x1_traj);
Longueur_traj = 0;
for i=2:Nb_point
  dist = distance(x1_traj(i-1),x2_traj(i-1),x1_traj(i),x2_traj(i));
  Longueur_traj = Longueur_traj + dist;
end;
Nb_obs = round(Longueur_traj/d_step);
% Interpolation de la trajectoire et rafinemment des points 
t   = 1:Nb_point;        
ts  = linspace(1,Nb_point,Nb_obs);    
p1  = interp1(t,x1_traj,ts);
p2  = interp1(t,x2_traj,ts);

