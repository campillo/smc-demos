%==========================================================================
% SMC demos [Sequential Monte Carlo demos]
%--------------------------------------------------------------------------
%  (C) Fabien Campillo
%  Fabien.Campillo@inria.fr                                   
%  INRIA                                    
%  version 1.0 - March 2009               
%--------------------------------------------------------------------------
% This set of matlab codes proposes some basic applications of sequential
% Monte Carlo (particle filtering). 
%--------------------------------------------------------------------------
% See LEGAL NOTICE (LEGAL-NOTICE.txt) in this directory.
%==========================================================================


function f_pause_plot()
% function f_pause_plot()
% pauses and makes a snapshot of the graphic screen
answer1 = input('   +++ pause/plot : a graphic output ? y/n [n] ','s');
if isempty(answer1),  answer1 = 'n'; , end

if answer1=='y'
  fprintf(['     output format ?\n' ...
	   '         1. jpeg \n' ...
	   '         2. tiff \n' ...
	   '         3. eps BW \n' ...
	   '         4. eps color \n']);

  answer2=input('      choice [1] : ','s');
  answer3=input('      file name : ','s');
  if isempty(answer2),  answer2 = '1';     , end
  if isempty(answer3),  answer3 = 'image'; , end
  
  switch answer2
   case '1'
    eval(['print -djpeg ' answer3,'.jpeg']);
   case '2'
    eval(['print -dtiff ' answer3,'.tiff']);
   case '3'
    eval(['print -deps2 ' answer3,'.eps']);
   case '4'
    eval(['print -depsc2 ' answer3,'.eps']);
  end
  
end
