# SMC demos

More details in this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/smc-demos/index.html).

## Description
A set of matlab applications of particle filtering for applications in tracking.
It is associated with the document [premiers\_pas\_en\_filtrage_particulaire.pdf](premiers_pas_en_filtrage_particulaire.pdf) (soon also written in English).

Sequential Monte Carlo (SMC) methods, also known as particle filters, are a class of algorithms for approximate inference in dynamic models. They are used to estimate the state of a system over time, given a sequence of noisy measurements. The basic idea of SMC methods is to represent the probability distribution over the state of the system at each time step using a set of weighted samples, or particles. The particles are propagated through time using the dynamics of the system, and their weights are updated based on the measurements that are made. The weights are used to approximate the true distribution, and they can be used to compute various statistics of interest, such as the mean and covariance of the state. SMC methods are useful because they allow us to make inferences about the state of a system in a computationally efficient manner, without requiring us to compute the true distribution explicitly. They have been applied in many fields, including engineering, computer science, and finance.


## Visuals
See this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/smc-demos/index.html).


## Usage

Just run `demo.m` under `matlab`.

## Roadmap
The author also plans a rewrite in Python.



## Authors and acknowledgment

[Zhang Qinghua](http://people.rennes.inria.fr/Qinghua.Zhang/) gave me a nice and 
efficient help in writing the function `f_resample_indices` in `f_resample_indices.m`. Thanks to him !

## License
[License](License.md)


## Project status
SMC demos is written with an ''old'' matlab, an update in a recent matlab would be welcome. 



## AUTHOR

SMC Author: 
[Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html), [MathNeuro Team](https://team.inria.fr/mathneuro/), [Inria centre
at Université Côte d’Azur](https://www.inria.fr/en/inria-centre-universite-cote-azur)
Fabien.Campillo@inria.fr
